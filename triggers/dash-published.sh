#!/bin/bash
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$script_dir/../isha-config.sh"

on_die ()
{
    # kill all children
    pkill -P $$
}

trap 'on_die' TERM
$FFMPEGSOURCE/ffmpeg -loglevel error -re -i rtmp://127.0.0.1:1935/dash/$1 -c:v copy -c:a copy -f flv rtmp://127.0.0.1:1935/hls/$1 2>>$LOGDIR/isha-monitoring.log &
wait
