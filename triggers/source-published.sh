#!/bin/bash
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$script_dir/../isha-common.sh" || true

SN=$1
SRC_APP=$2
SOURCE=rtmp://localhost:1935/$SRC_APP/$SN
DEST="rtmp://localhost:1935/receive"
PROGRESSFILE=$LOGDIR/${SN}_forward_progress.log
slog=$LOGDIR/${SN}_forward.log

msg="Forwarding stream $SN to receive from source-published.."
log_info "$msg" && echo "$msg" >> $slog


# Clear placeholder if exists

placeholder_pidfile=$(placeholder_pid_filename $SN)
echo "Ensuring process from $placeholder_pidfile is cleared" >> $slog
ensure_pidfile_cleared_or_fail $placeholder_pidfile

# Ensure nothing else is streaming

lockfile=$(lockfile_for_sn_and_dest $SN $DEST)
echo "Attempting to acquire lockfile $lockfile" >> $slog
acquire_lock_or_fail $lockfile

on_die ()
{
		echo "Exiting source-published.." >> $LOGDIR/${SN}_forward.log
		
		# Release lock
		# This will anyway get released even if process gets killed with -9
		# But would have around 30 seconds of delay varying by OS
		flock --unlock 4

    pkill -P $$ 2>>$LOGDIR/isha-monitoring.log 1>>$slog
}
trap 'on_die' EXIT

cmd="$FFMPEGSOURCE/ffmpeg -hide_banner -loglevel error \
-progress $PROGRESSFILE -nostdin \
-re -i $SOURCE \
-map 0:a -c:a copy -map 0:v -c:v copy -max_muxing_queue_size 1024 -f flv $DEST/${SN}" 

echo "Executing forward with $cmd" >> $LOGDIR/${SN}_forward.log

$cmd 2>>$LOGDIR/isha-monitoring.log &
wait
