declare -A TIMINGS_1day_t1_session11=(
["stream_start"]=2021-03-09T13:45:00+05:30
["session_start"]=2021-03-09T14:30:00+05:30
["session_end"]=2021-03-09T15:30:00+05:30
["stream_end"]=2021-03-09T16:00:00+05:30
)

declare -A TIMINGS_3day_t1_session1=(
["stream_start"]=2021-03-09T05:45:00+05:30
["session_start"]=2021-03-09T06:30:00+05:30
["session_end"]=2021-03-09T10:30:00+05:30
["stream_end"]=2021-03-09T11:00:00+05:30
)
declare -A TIMINGS_3day_t1_session2=(
["stream_start"]=2021-03-09T19:45:00+05:30
["session_start"]=2021-03-09T20:30:00+05:30
["session_end"]=2021-03-10T21:30:00+05:30
["stream_end"]=2021-03-10T22:00:00+05:30
)
declare -A TIMINGS_3day_t1_session3=(
["stream_start"]=2021-03-10T05:45:00+05:30
["session_start"]=2021-03-10T06:30:00+05:30
["session_end"]=2021-03-10T10:30:00+05:30
["stream_end"]=2021-03-10T11:00:00+05:30
)
declare -A TIMINGS_3day_t1_session4=(
["stream_start"]=2021-03-10T19:45:00+05:30
["session_start"]=2021-03-10T20:30:00+05:30
["session_end"]=2021-03-10T21:30:00+05:30
["stream_end"]=2021-03-10T22:00:00+05:30
)
declare -A TIMINGS_3day_t1_session5=(
["stream_start"]=2021-03-11T05:45:00+05:30
["session_start"]=2021-03-11T06:30:00+05:30
["session_end"]=2021-03-11T09:30:00+05:30
["stream_end"]=2021-03-11T10:00:00+05:30
)
declare -A TIMINGS_3day_t2_session1=(
["stream_start"]=2021-03-09T10:15:00+05:30
["session_start"]=2021-03-09T11:00:00+05:30
["session_end"]=2021-03-09T15:00:00+05:30
["stream_end"]=2021-03-09T15:30:00+05:30
)
declare -A TIMINGS_3day_t2_session2=(
["stream_start"]=2021-03-09T23:15:00+05:30
["session_start"]=2021-03-10T00:00:00+05:30
["session_end"]=2021-03-10T01:00:00+05:30
["stream_end"]=2021-03-10T01:30:00+05:30
)
declare -A TIMINGS_3day_t2_session3=(
["stream_start"]=2021-03-10T10:15:00+05:30
["session_start"]=2021-03-10T11:00:00+05:30
["session_end"]=2021-03-10T15:00:00+05:30
["stream_end"]=2021-03-10T15:30:00+05:30
)
declare -A TIMINGS_3day_t2_session4=(
["stream_start"]=2021-03-10T23:15:00+05:30
["session_start"]=2021-03-11T00:00:00+05:30
["session_end"]=2021-03-11T01:00:00+05:30
["stream_end"]=2021-03-11T01:30:00+05:30
)
declare -A TIMINGS_3day_t2_session5=(
["stream_start"]=2021-03-11T10:15:00+05:30
["session_start"]=2021-03-11T11:00:00+05:30
["session_end"]=2021-03-11T14:00:00+05:30
["stream_end"]=2021-03-11T14:30:00+05:30
)
declare -A TIMINGS_3day_t3_session1=(
["stream_start"]=2021-03-09T11:45:00+05:30
["session_start"]=2021-03-09T12:30:00+05:30
["session_end"]=2021-03-09T16:30:00+05:30
["stream_end"]=2021-03-09T17:00:00+05:30
)
declare -A TIMINGS_3day_t3_session2=(
["stream_start"]=2021-03-09T23:45:00+05:30
["session_start"]=2021-03-10T00:30:00+05:30
["session_end"]=2021-03-10T01:30:00+05:30
["stream_end"]=2021-03-10T02:00:00+05:30
)
declare -A TIMINGS_3day_t3_session3=(
["stream_start"]=2021-03-10T11:45:00+05:30
["session_start"]=2021-03-10T12:30:00+05:30
["session_end"]=2021-03-10T16:30:00+05:30
["stream_end"]=2021-03-10T17:00:00+05:30
)
declare -A TIMINGS_3day_t3_session4=(
["stream_start"]=2021-03-10T23:45:00+05:30
["session_start"]=2021-03-11T00:30:00+05:30
["session_end"]=2021-03-11T01:30:00+05:30
["stream_end"]=2021-03-11T02:00:00+05:30
)
declare -A TIMINGS_3day_t3_session5=(
["stream_start"]=2021-03-11T11:45:00+05:30
["session_start"]=2021-03-11T12:30:00+05:30
["session_end"]=2021-03-11T15:30:00+05:30
["stream_end"]=2021-03-11T16:00:00+05:30
)
declare -A TIMINGS_3day_t4_session1=(
["stream_start"]=2021-03-09T14:45:00+05:30
["session_start"]=2021-03-09T15:30:00+05:30
["session_end"]=2021-03-09T19:30:00+05:30
["stream_end"]=2021-03-09T20:00:00+05:30
)
declare -A TIMINGS_3day_t4_session2=(
["stream_start"]=2021-03-10T02:45:00+05:30
["session_start"]=2021-03-10T03:30:00+05:30
["session_end"]=2021-03-10T04:30:00+05:30
["stream_end"]=2021-03-10T05:00:00+05:30
)
declare -A TIMINGS_3day_t4_session3=(
["stream_start"]=2021-03-10T14:45:00+05:30
["session_start"]=2021-03-10T15:30:00+05:30
["session_end"]=2021-03-10T19:30:00+05:30
["stream_end"]=2021-03-10T20:00:00+05:30
)
declare -A TIMINGS_3day_t4_session4=(
["stream_start"]=2021-03-11T02:45:00+05:30
["session_start"]=2021-03-11T03:30:00+05:30
["session_end"]=2021-03-11T04:30:00+05:30
["stream_end"]=2021-03-11T05:00:00+05:30
)
declare -A TIMINGS_3day_t4_session5=(
["stream_start"]=2021-03-11T14:45:00+05:30
["session_start"]=2021-03-11T15:30:00+05:30
["session_end"]=2021-03-11T18:30:00+05:30
["stream_end"]=2021-03-11T19:00:00+05:30
)
declare -A TIMINGS_3day_t5_session1=(
["stream_start"]=2021-03-10T03:45:00+05:30
["session_start"]=2021-03-10T04:30:00+05:30
["session_end"]=2021-03-10T08:30:00+05:30
["stream_end"]=2021-03-10T09:00:00+05:30
)
declare -A TIMINGS_3day_t5_session2=(
["stream_start"]=2021-03-10T18:45:00+05:30
["session_start"]=2021-03-10T19:30:00+05:30
["session_end"]=2021-03-10T20:30:00+05:30
["stream_end"]=2021-03-10T21:00:00+05:30
)
declare -A TIMINGS_3day_t5_session3=(
["stream_start"]=2021-03-11T03:45:00+05:30
["session_start"]=2021-03-11T04:30:00+05:30
["session_end"]=2021-03-11T08:30:00+05:30
["stream_end"]=2021-03-11T09:00:00+05:30
)
declare -A TIMINGS_3day_t5_session4=(
["stream_start"]=2021-03-11T18:45:00+05:30
["session_start"]=2021-03-11T19:30:00+05:30
["session_end"]=2021-03-11T20:30:00+05:30
["stream_end"]=2021-03-11T21:00:00+05:30
)
declare -A TIMINGS_3day_t5_session5=(
["stream_start"]=2021-03-12T03:45:00+05:30
["session_start"]=2021-03-12T04:30:00+05:30
["session_end"]=2021-03-12T07:30:00+05:30
["stream_end"]=2021-03-12T08:00:00+05:30
)

declare -A TIMINGS_1day_t2_session11=(
["stream_start"]=2021-03-11T10:15:00+05:30
["session_start"]=2021-03-11T11:00:00+05:30
["session_end"]=2021-03-11T12:00:00+05:30
["stream_end"]=2021-03-11T12:30:00+05:30
)
declare -A TIMINGS_1day_t3_session11=(
["stream_start"]=2021-03-11T11:45:00+05:30
["session_start"]=2021-03-11T12:30:00+05:30
["session_end"]=2021-03-11T13:30:00+05:30
["stream_end"]=2021-03-11T14:00:00+05:30
)
declare -A TIMINGS_1day_t4_session11=(
["stream_start"]=2021-03-11T14:45:00+05:30
["session_start"]=2021-03-11T15:30:00+05:30
["session_end"]=2021-03-11T16:30:00+05:30
["stream_end"]=2021-03-11T17:00:00+05:30
)
declare -A TIMINGS_1day_t5_session11=(
["stream_start"]=2021-03-12T03:45:00+05:30
["session_start"]=2021-03-12T04:30:00+05:30
["session_end"]=2021-03-12T05:30:00+05:30
["stream_end"]=2021-03-12T06:00:00+05:30
)