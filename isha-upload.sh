#!/bin/bash

usage() { 
  # stream name: "fmf_stream2_session1"
  # 
    echo -e "usage: isha-upload \n\
    -s <session identifier from stream definitions without individual stream name eg. 'fmf_t1_session1'>\n\
    [-c <number of seconds to cut from start, default 2520=42 minutes>]\n\
    [-k (keep cut files in $RECDIR/s3)]
    [-e <comma separated list of languages to exclude eg. 'stream3,stream5'. Note: no spaces!>]" >&2
  exit 1; 
}

FROM=2520
KEEP_TEMP_FILES=0
declare -ga EXCLUDE_STREAMS

while getopts ":s:c:e:rk" o; do
    case "${o}" in
        s)
            SID=${OPTARG}
            ;;
        c)
            FROM=${OPTARG}
            ;;
        k)
            KEEP_TEMP_FILES=1
            ;;
        e)
            # split by comma into array
            IFS=',' read -r -a EXCLUDE_STREAMS <<< ${OPTARG}
            ;;
        *)
            echo "Unknown param: $o"
            usage
            ;;  
    esac
done
shift $((OPTIND-1))

if [ -z "${SID}" ]; then
    usage
fi

function upload_stream() {
    SN=$1
    FROM=$2
    RETRANSCODE=$3
    KEEP_TEMP_FILES=$4
    SRC_EXT=$5
    DIRECTLY_PACKAGE=$6
    OUTPUT_FILENAME=$7

    FFMPEG_FROM="-ss $FROM"

    # PSEUDOCODE: cut the video

    # EXT1="_bad"
    # EXT2="_low"
    # EXT3="_med"
    # EXT4="_high"
    # EXT5="_best"
    # EXT6="_max"

    PROGRESSFILE=$LOGDIR/${SN}_upload_progress.log
    LOGFILE=$LOGDIR/${SN}_upload.log

    on_die()
    {
        pkill -P $$
        exit 1
    }
    trap 'on_die' EXIT SIGINT

    IN1=${RECDIR}/s3/${SN}.mp4

    cmd="$FFMPEGSOURCE/ffmpeg -y -hide_banner -loglevel error -nostdin -progress $PROGRESSFILE $FFMPEG_FROM -i $RECDIR/${SN}${SRC_EXT} \
    -map 0:v -map 0:a -c copy -f mp4 ${IN1}"

    if [ ${DIRECTLY_PACKAGE} -ne 1 ]; then
        echo "Starting S3 upload for ${SN}: cutting each video for $FROM s from the start, command $cmd"
        echo "Starting S3 upload for ${SN}: cutting each video for $FROM s from the start, command $cmd" >> $LOGFILE    
        log_info "Starting S3 upload for ${SN}: cutting each video for $FROM s removed from the start"

        $cmd
    fi

    # THEN RUN transcode
    cmd="aws s3 cp ${IN1} s3://ishafullmoon"

    log_info "Upload step for ${SN}"
    echo -e "\n$(date +%c) Upload step for ${SN}: $cmd"
    echo -e "\n$(date +%c) Upload step for: ${SN}: $cmd" >> $PROGRESSFILE
    
    $cmd 2>>$LOGDIR/isha-monitoring.log

    echo "$(date +%c) Output: $output"
    # if [ "$packager_output" == "Packaging completed successfully." ]; then
    #     echo " Successfully packaged ${SN}"
    #     echo " Successfully packaged ${SN}" >> $LOGFILE
    # else
    #     handle_error "Had a problem packaging $SN: $packager_output"
    # fi

    if [ "${KEEP_TEMP_FILES}" -ne 1 ]; then
        rm "${RECDIR}/s3/${SN}.mp4"
    fi
}

upload_individual_stream() {
  local SN=$1

  bold=$(tput bold)
  normal=$(tput sgr0)

  for EXCLUDE_STREAM in "${EXCLUDE_STREAMS[@]}"; do
    EXCLUDE_STREAM=$(populate_session_id_from_language_string $SID $EXCLUDE_STREAM)
    if [ $SN == $EXCLUDE_STREAM ]; then
      echo -e "\n${bold}# EXCLUDED: Skipping excluded stream ${SN}${normal}"
      return 1
    fi
  done

  echo -e "\n${bold}# Uploading stream: ${SN}${normal}"
  upload_stream $SN $FROM 0 $KEEP_TEMP_FILES '.ts' 0 

  echo -e "\n"
}

function upload_all() {
    parse_sdata $SID

    echo "$(timestamp) session $SID in status ${sdata['status']}"

    for SN in "${sdata_streams[@]}"; do
      echo "$(timestamp) going through stream $SN"
      
      # Check are we currently streaming something?
      live_status=$(process_hls_status $SN)
      if [[ $live_status == 'ok' ]]; then
        echo "Session is currently live"
        #validate_stream $SN

      else # We are offline
        echo "Session not currently live"
      fi

      # TODO: should we do something based on if a session is still currently streaming
      
      upload_individual_stream $SN &
    done
    wait
}

# Autoexecute if being called directly
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]
then
  script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  source "$script_dir/isha-common.sh" || true

  upload_all $
fi
