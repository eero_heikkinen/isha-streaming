#!/bin/bash

usage() { 
  # stream name: "fmf_stream2_session1"
  # 
    echo -e "usage: isha-transcode-all.sh \n\
    -s <session identifier from stream definitions without individual stream name eg. 'fmf_t1_session1'>\n\
    [-c <number of seconds to cut from start, default 2520=42 minutes>]\n\
    [-e <comma separated list of languages to exclude eg. 'stream3,stream5'. Note: no spaces!>]" >&2
  exit 1; 
}

FROM=2520
RETRANSCODE=0
declare -ga EXCLUDE_STREAMS

while getopts ":s:c:e:r" o; do
    case "${o}" in
        s)
            SID=${OPTARG}
            ;;
        c)
            FROM=${OPTARG}
            ;;
        r)
            RETRANSCODE=1
            ;;
        e)
            # split by comma into array
            IFS=',' read -r -a EXCLUDE_STREAMS <<< ${OPTARG}
            ;;
        *)
            echo "Unknown param: $o"
            usage
            ;;  
    esac
done
shift $((OPTIND-1))

if [ -z "${SID}" ]; then
    usage
fi

process_individual_stream() {
  local SN=$1

  bold=$(tput bold)
  normal=$(tput sgr0)

  for EXCLUDE_STREAM in "${EXCLUDE_STREAMS[@]}"; do
    EXCLUDE_STREAM=$(populate_session_id_from_language_string $SID $EXCLUDE_STREAM)
    if [ $SN == $EXCLUDE_STREAM ]; then
      echo -e "\n${bold}# EXCLUDED: Skipping excluded stream ${SN}${normal}"
      return 1
    fi
  done

  echo -e "\n${bold}# Processing stream: ${SN}${normal}"
  
  IN1=${RECDIR}/${SN}.ts

  cmd="$script_dir/isha-transcode.sh $SN file ${IN1} $FROM"

  echo -e "\n$(date +%c) Processing step for ${SN}: $cmd"
    
  $cmd &
  wait

  echo "$(date +%c) Output: $output"
  echo -e "\n"
}

function process_all() {
    parse_sdata $SID

    echo "$(timestamp) session $SID in status ${sdata['status']}"

    for SN in "${sdata_streams[@]}"; do
      echo "$(timestamp) going through stream $SN"
      
      # Check are we currently streaming something?
      live_status=$(process_hls_status $SN)
      if [[ $live_status == 'ok' ]]; then
        echo "Session is currently live"
        #validate_stream $SN

      else # We are offline
        echo "Session not currently live"
      fi

      # TODO: should we do something based on if a session is still currently streaming
      
      process_individual_stream $SN &
      sleep 5
    done
}

# Autoexecute if being called directly
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]
then
  script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
  source "$script_dir/isha-common.sh" || true

  process_all
fi
