#!/bin/bash
usage() { 
	echo "usage: isha-vbr -s <source stream name> -t <target stream name> [-f <from>] [-l]" >&2;
	#e.g $1 = stream1_session1 ; $2 = rtmp://a.youtube.com/live/2/ ; $3 = t1
	exit 1; 
}

while getopts ":s:t:f:l" o; do
    case "${o}" in
        s)
            source=${OPTARG}
            ;;
        t)
            target=${OPTARG}
            ;;
        l)
            loop=1
            ;;
        f)
            from=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${source}" ] || [ -z "${target}" ]; then
    usage
fi

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$script_dir/isha-common.sh" || true

RECORDDIR=$RECDIR
#EXT0="_worst.ts"
EXT1="_bad.ts"
EXT2="_low.ts"
EXT3="_med.ts"
EXT4="_high.ts"
EXT5="_best.ts"
EXT6="_max.ts"
DEST=rtmp://localhost:1935/dash

FFMPEG_FROM=""
if [[ -n $from ]]; then
	FFMPEG_FROM="-ss $from"
fi

FFMPEG_LOOP=""
if [[ -n $loop ]]; then
	FFMPEG_LOOP="-stream_loop -1"
fi

PROGRESSFILE=$LOGDIR/${target}_player_progress.log
slog=$LOGDIR/${target}_delayed.log

log_info "VBR player initiated with params $source $target $FFMPEG_FROM $FFMPEG_LOOP"

if [ ! -e ${RECDIR}/${source}_max.ts ]	||\
	 [ ! -e ${RECDIR}/${source}_best.ts ] ||\
	 [ ! -e ${RECDIR}/${source}_high.ts ] ||\
	 [ ! -e ${RECDIR}/${source}_med.ts ] ||\
	 [ ! -e ${RECDIR}/${source}_low.ts ] ||\
	 [ ! -e ${RECDIR}/${source}_bad.ts ]; then
# ||\
#	 [ ! -e ${RECDIR}/${source}_worst.ts ]; then
	 	handle_error "One of the input files in isha-vbr for $source -> $target is missing"
fi

until [ -f $RECORDDIR/${source}${EXT3} ]
do
     sleep 2
     echo "Waiting for a file to play to become available..." >> $slog
done

#### Lockfile logic

lock_destination=rtmp://localhost:1935/receive
lockfile=$(lockfile_for_sn_and_dest $target $lock_destination ) || {
    msg="While determining lockfile: $lockfile"
    handle_error "$msg" && echo "$msg"
    exit 1
}
echo "Attempting to acquire lockfile $lockfile" >> $slog
acquire_lock_or_fail $lockfile

on_die ()
{
    flock --unlock 4
	log_info "VBR player stopped playing $source to $target"
	  # ensure ffmpeg stops with parent
    pkill -P $$
}
trap 'on_die' EXIT

cmd="$FFMPEGSOURCE/ffmpeg -hide_banner -loglevel error -nostdin $FFMPEG_LOOP -progress $PROGRESSFILE -re $FFMPEG_FROM -i $RECORDDIR/${source}${EXT1} $FFMPEG_FROM -i $RECORDDIR/${source}${EXT2} $FFMPEG_FROM -i $RECORDDIR/${source}${EXT3} $FFMPEG_FROM -i $RECORDDIR/${source}${EXT4} $FFMPEG_FROM -i $RECORDDIR/${source}${EXT5} $FFMPEG_FROM -i $RECORDDIR/${source}${EXT6} \
-map 0:v -map 0:a -c copy -f flv ${DEST}/${target}_bad \
-map 1:v -map 1:a -c copy -f flv ${DEST}/${target}_low \
-map 2:v -map 2:a -c copy -f flv ${DEST}/${target}_med \
-map 3:v -map 3:a -c copy -f flv ${DEST}/${target}_high \
-map 4:v -map 4:a -c copy -f flv ${DEST}/${target}_best \
-map 5:v -map 5:a -c copy -f flv ${DEST}/${target}_max"

echo "$($DATE +%c) Starting streaming with $cmd" >> $slog
echo "$cmd"

$cmd >> $slog &
wait

echo "$($DATE +%c) Finished streaming with exit code $?" >> $slog
