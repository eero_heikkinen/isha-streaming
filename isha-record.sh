#!/bin/bash

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$script_dir/isha-common.sh" || true

SN=$1 #stream name eg. program_tz_languageX_sessionY
APP=$2 # this is the rtmp application to record from eg. "distribute"
loglevel=${3:-} # if loglevel is "silent" skip some logging messages

SOURCE=rtmp://localhost:1935/$APP/$SN
RECFILE=$RECDIR/${SN}.ts
PROGRESSFILE=$LOGDIR/${SN}_record_progress.log
slog=$LOGDIR/${SN}_record.log

parse_sdata $SN

if [[ -z ${sdata[@]-} ]]; then
 ! log_info "Unrecognized session $SN - Recording"
else
	# There is a recognized session
	if [[ -n ${sdata["delayed"]-} ]] && [[ ${sdata["delayed"]} == 'true' ]]; then
		! log_info "Delayed stream $SN - sleeping"
		sleep 60
		exit 0
	elif [[ ${sdata["status"]} == 'test' ]]; then
		#if [[ "$loglevel" != 'silent' ]]; then
			message="Not recording stream id: $SN $APP - outside scheduled time"
			! log_info "$message"
			echo $message
		#fi

		sleep 60
		exit 0
	fi
fi

on_die ()
{
		if [ "$loglevel" != 'silent' ]; then
			! recording_stopped $SN
		fi
    # kill all children
    pkill -P $$ 2>>$slog 1>>$slog
}
trap 'on_die' EXIT

if [ "$loglevel" != 'silent' ]; then
	! recording_started $SN $CURRENT_RECORDED_SECONDS || true
fi

APARAM0="-map 0:a -c:a copy"
VPARAM0="-map 0:v -c:v copy"
RPARAM0="-max_muxing_queue_size 1024 -f mpegts -"

cmd="$FFMPEGSOURCE/ffmpeg -hide_banner -loglevel error \
-progress $PROGRESSFILE -nostdin \
-re -i $SOURCE $APPENDPARAM \
$APARAM0 $VPARAM0 $RPARAM0"

echo "$(date +%c) Recording from $APP/$SN to $RECFILE with $cmd" >> $slog

$cmd 2>>$slog >>$RECFILE &
wait

echo "$(date +%c) Finished recording from $APP/$SN to $RECFILE" >> $slog