#!/bin/bash

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$script_dir/isha-common.sh"

handle_delayed_stream_offline() {
  local SN=$1

  echo "Delayed stream offline: $SN"

  # Automatic playing of delayed streams not implemented
}

handle_live_stream_offline() {
  local SN=$1
  
  duration=$(current_offset_from_section $(remove_language SN) session_end)
  duration=$(( duration * -1 ))

  if [[ duration -le 0 ]]; then
    log_info "Live stream offline but already past session_end; not spawning placeholder"
    return 0
  fi
  
  cmd="nohup $TIMEOUT $duration /opt/isha-streaming/isha-rtmp.sh -p -d rtmp://localhost:1935/receive -t $SN -l"
  msg="Playing placeholder for $SN with duration $duration, cmd $cmd"
  log_info "$msg" && echo "$(timestamp) $msg"

  $cmd >>$LOGDIR/${SN}_placeholder.log 2>&1 &
}

function validate_stream() {
  local SN=$1

  echo "$(timestamp) Validating $SN"

  if player_active $SN && [[ ${sdata['delayed']} != 'true' ]]; then
    if [[ ${sdata['status']} == 'session' ]]; then
      handle_error "Live stream $SN playing placeholder video during session"
    else
      log_info "Live stream $SN playing placeholder video"
    fi
  fi

  # TODO verify stream more closely
}

function iterate_sessions() {
  ONGOING_SESSIONS=`ongoing_sessions`

  [[ -z $ONGOING_SESSIONS ]] && echo "No ongoing sessions"

  for session_id in ${ONGOING_SESSIONS}; do
    parse_sdata $session_id

    echo "$(timestamp) session $session_id in status ${sdata['status']}"

    for SN in "${sdata_streams[@]}"; do
      echo "$(timestamp) going through stream $SN"
      
      # Check are we currently streaming something?
      live_status=$(process_hls_status $SN)
      if [[ $live_status == 'ok' ]]; then
        validate_stream $SN

      else # We are offline
        log_info "$SN is not online but expected to be on: reacting"
        
        if [[ ${sdata['status']} == 'session' ]]; then
          # Report error
          handle_error "$SN not online during session"
        fi

        if [[ ${sdata['delayed']} == 'true' ]]; then
          handle_delayed_stream_offline $SN
        else
          handle_live_stream_offline $SN
        fi
      fi
    done
  done
}

# Autoexecute if being called directly
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]
then
  iterate_sessions
fi