#!/bin/bash

#set -o errexit
set +o nounset
set -o pipefail

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$script_dir/isha-common.sh" || true

RECORDDIR=$RECDIR
#EXT0="_worst.ts"
EXT1="_bad.ts"
EXT2="_low.ts"
EXT3="_med.ts"
EXT4="_high.ts"
EXT5="_best.ts"
EXT6="_max.ts"
DEST=rtmp://localhost:1935/dash

# e.g $1 = stream1_session1 ; $2 = rtmp://a.youtube.com/live/2/ ; $3 = t1

function play_from_recorded_transcodings() {
	local parent_sn=$1
	local target_sn=$2

	log_info "Delayed stream initiated with params $1 $2"

	PROGRESSFILE=$LOGDIR/${target_sn}_player_progress.log
	
	# WIP: implement lockfiles so can keep calling this script
	#parse_sdata $session_id
  #LOCKFILE=/tmp/isha.$session_id.lock
	#if [ ! -f $LOGFILE ]; then
	#	log_info "lockfile exists for $session_id, quitting"
  #	exit 0
	#fi

	slog=$LOGDIR/${2}_delayed.log

	touch $slog
	echo "" > $slog

	local t1=$($DATE +%c)
	echo "${t1}" >> $slog

	### now that we have multiple res streams for a language - add code for streaming all res files of that stream_session
	## get session number from the parameters passed to the script - session1

	until [ -f $RECORDDIR/${1}${EXT1} ]
	do
	     sleep 2
	     echo "rechecking..." >> $slog
	done

	local t2=$($DATE +%c)
	echo "${t2}" >> $slog
	echo "Starting streaming" >> $slog

	FFMPEG_OFFSET=

	offset=$(calculate_offset $1) || 0

	if [ "$offset" -gt "0" ]; then
		echo "Going to sleep for $offset seconds"
	  sleep $offset
    # TODO
    # play_placeholder_video $offset
	  # -i placeholder image -t duration 
	else
		local offset_inverse=$((offset * -1))
	  FFMPEG_OFFSET="-ss $offset_inverse" 
	fi

	echo $FFMPEG_OFFSET

	echo $RECORDDIR/${1}${EXT1} >> $slog
echo $FFMPEGSOURCE/ffmpeg -hide_banner -loglevel error -nostdin $FFMPEG_OFFSET -progress $PROGRESSFILE -re -i $RECORDDIR/${1}${EXT1} -i $RECORDDIR/${1}${EXT2} -i $RECORDDIR/${1}${EXT3} -i $RECORDDIR/${1}${EXT4} -i $RECORDDIR/${1}${EXT5} -i $RECORDDIR/${1}${EXT6} \
	-map 0:v -map 0:a -c copy -f flv ${DEST}/$2_bad \
	-map 1:v -map 1:a -c copy -f flv ${DEST}/$2_low \
	-map 2:v -map 2:a -c copy -f flv ${DEST}/$2_med \
	-map 3:v -map 3:a -c copy -f flv ${DEST}/$2_high \
	-map 4:v -map 4:a -c copy -f flv ${DEST}/$2_best \
	-map 5:v -map 5:a -c copy -f flv ${DEST}/$2_max	

$FFMPEGSOURCE/ffmpeg -hide_banner -loglevel error -nostdin $FFMPEG_OFFSET -progress $PROGRESSFILE -re -i $RECORDDIR/${1}${EXT1} -i $RECORDDIR/${1}${EXT2} -i $RECORDDIR/${1}${EXT3} -i $RECORDDIR/${1}${EXT4} -i $RECORDDIR/${1}${EXT5} -i $RECORDDIR/${1}${EXT6} -t 02:45:52 \
	-map 0:v -map 0:a -c copy -f flv ${DEST}/$2_bad \
	-map 1:v -map 1:a -c copy -f flv ${DEST}/$2_low \
	-map 2:v -map 2:a -c copy -f flv ${DEST}/$2_med \
	-map 3:v -map 3:a -c copy -f flv ${DEST}/$2_high \
	-map 4:v -map 4:a -c copy -f flv ${DEST}/$2_best \
	-map 5:v -map 5:a -c copy -f flv ${DEST}/$2_max
	echo "done" >> $slog
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]
then
  play_from_recorded_transcodings $1 $2 &
fi
