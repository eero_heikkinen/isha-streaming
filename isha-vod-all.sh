#!/bin/bash

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$script_dir/isha-common.sh" || true

source "$script_dir/isha-vod.sh"

#### Parse params

usage() { 
  echo -e "isha-vod-all.sh will read all streams for a particular session identifier from the stream definitions and package all of them for vod. The recorded variant files are expected to be found in ${RECDIR}, or if the -r re-encoding option is provided the master file is expected to be found there, which is then used to transcode the quality variants prior to packaging.\n\
  usage: isha-vod-all.sh \n\
  -s <session identifier from stream definitions without individual stream name eg. 'fmf_t1_session1'>\n\
  [-d (directly package without cutting)]
  [-e <comma separated list of stream names to exclude. Note: no spaces!>]
  [-r (re-encode using gpu from master file)]
  [-c <number of seconds to cut from start, default 2520=42 minutes>]
  OR: -c"
  exit 1; 
}

# Defaults
FROM=2520
RETRANSCODE=0
DIRECTLY_PACKAGE=0
declare -ga EXCLUDE_STREAMS

while getopts ":s:c:e:rd" o; do
    case "${o}" in
        s)
            SID=${OPTARG}
            ;;
        c)
            FROM=${OPTARG}
            ;;
        d)
            DIRECTLY_PACKAGE=1
            ;;
        r)
            RETRANSCODE=1
            ;;
        e)
            # split by comma into array
            IFS=',' read -r -a EXCLUDE_STREAMS <<< ${OPTARG}
            ;;
        *)
            echo "Unknown param: $o"
            usage
            ;;  
    esac
done
shift $((OPTIND-1))

if [ -z "${SID}" ]; then
    usage
fi

package_individual_stream() {
  local SN=$1

  bold=$(tput bold)
  normal=$(tput sgr0)

  for EXCLUDE_STREAM in "${EXCLUDE_STREAMS[@]}"; do
    EXCLUDE_STREAM=$(populate_session_id_from_language_string $SID $EXCLUDE_STREAM)
    if [ $SN == $EXCLUDE_STREAM ]; then
      echo -e "\n${bold}# EXCLUDED: Skipping excluded stream ${SN}${normal}"
      return 1
    fi
  done

  echo -e "\n${bold}# Packaging stream with isha-vod.sh: ${SN}${normal}"

  #package_stream $SN $FROM $RETRANSCODE $KEEP_TEMP_FILES $SRC_EXT $DIRECTLY_PACKAGE $OUTPUT_FILENAME
  package_stream $SN $FROM $RETRANSCODE $DIRECTLY_PACKAGE ".ts"    $DIRECTLY_PACKAGE $SN
  echo -e "\n"
}

function package_all() {
    parse_sdata $SID

    echo "$(timestamp) session $SID in status ${sdata['status']}"

    for SN in "${sdata_streams[@]}"; do
      echo "$(timestamp) going through stream $SN"
      
      # Check are we currently streaming something?
      live_status=$(process_hls_status $SN)
      if [[ $live_status == 'ok' ]]; then
        echo "Session is currently live"
        #validate_stream $SN

      else # We are offline
        echo "Session not currently live"
      fi

      # TODO: should we do something based on if a session is still currently streaming
      
      package_individual_stream $SN &
      sleep 5
    done
    wait
}

# Autoexecute if being called directly
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]
then
  package_all
fi
