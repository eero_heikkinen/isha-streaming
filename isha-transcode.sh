#!/bin/bash

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$script_dir/isha-common.sh" || true

#### Parse params

usage() { 
  # stream name: "fmf_stream2_session1"
  # 
	echo "usage: isha-transcode <stream name> <destination> [<file input>]" >&2
  exit 1; 
}

SN=$1
DESTINATION=$2
FILE_INPUT=$3
CUT_LENGTH=$4

if [ -z "${SN}" ] || [ -z "${DESTINATION}" ]; then
    usage
fi

if [[ $DESTINATION == 'dash' ]]; then
	# No file source: use default behaviour to read stream from receive
	SOURCE_PARAMS="-re -i rtmp://localhost:1935/receive/$SN"
	DEST="rtmp://localhost:1935/$DESTINATION"
#	o_worst="-f flv $DEST/${SN}_worst"
	o_med="-f flv $DEST/${SN}_med"
	o_best="-f flv $DEST/${SN}_best"
	o_bad="-f flv $DEST/${SN}_bad"
	o_low="-f flv $DEST/${SN}_low"
	o_high="-f flv $DEST/${SN}_high"
	o_max="-f flv $DEST/${SN}_max"
else 
	if [ -z $FILE_INPUT ]; then
		usage
	fi

	# We have a file source
	# Go into mode: Transcode into files
	
	if [ ! -z $CUT_LENGTH ]; then
		SOURCE_PARAMS="-ss $CUT_LENGTH -i $FILE_INPUT"
	#	o_worst="-f mpegts $RECDIR/${SN}_worst.ts"
		o_med="-f mp4 $RECDIR/${SN}_med_cut${CUT_LENGTH}.mp4"
		o_best="-f mp4 $RECDIR/${SN}_best_cut${CUT_LENGTH}.mp4"
		o_bad="-f mp4 $RECDIR/${SN}_bad_cut${CUT_LENGTH}.mp4"
		o_low="-f mp4 $RECDIR/${SN}_low_cut${CUT_LENGTH}.mp4"
		o_high="-f mp4 $RECDIR/${SN}_high_cut${CUT_LENGTH}.mp4"
		o_max="-f mp4 $RECDIR/${SN}_max_cut${CUT_LENGTH}.mp4"
		# Don't cut the original file
		# This is not needed in the usual case if our assumed goal is VOD packaging 
		#o_orig="-c copy -f mpegts $RECDIR/${SN}_${CUT_LENGTH}.ts"
	else
		# We have a file source
		# Go into mode: Transcode into files
		SOURCE_PARAMS="-i $FILE_INPUT"
	#	o_worst="-f mpegts $RECDIR/${SN}_worst.ts"
		o_med="-f mpegts $RECDIR/${SN}_med.ts"
		o_best="-f mpegts $RECDIR/${SN}_best.ts"
		o_bad="-f mpegts $RECDIR/${SN}_bad.ts"
		o_low="-f mpegts $RECDIR/${SN}_low.ts"
		o_high="-f mpegts $RECDIR/${SN}_high.ts"
		o_max="-f mpegts $RECDIR/${SN}_max.ts"
		o_orig="-c copy -f mpegts $RECDIR/${SN}.ts"
	fi
fi

log_info "STARTING TRANSCODE $SN $DESTINATION"
echo "STARTING TRANSCODE $SN $DESTINATION" >> $LOGDIR/${SN}_transcode.log

PROGRESSFILE=$LOGDIR/${SN}_transcode_progress.log

APARAM0="-map 0:a -c:a copy"
VPARAM0="-map 0:v -c:v copy"
RPARAM0="-max_muxing_queue_size 1024 -f mpegts - "

a_worst="-c:a libfdk_aac -ac 1 -b:a 32k"
a_bad="-c:a libfdk_aac -ac 1 -b:a 40k"
a_low="-c:a libfdk_aac -ac 2 -b:a 48k"
a_med="-c:a libfdk_aac -ac 2 -b:a 64k"
a_high="-c:a libfdk_aac -ac 2 -b:a 96k"
a_best="-c:a libfdk_aac -ac 2 -b:a 128k"

v_basew="-c:v libx264 -preset veryfast -tune zerolatency -keyint_min 60 -g 60 -sc_threshold 0 -r 15 -pix_fmt yuv420p"
v_base0="-c:v libx264 -preset veryfast -tune zerolatency -keyint_min 60 -g 60 -sc_threshold 0 -r 15 -pix_fmt yuv420p -crf 18"

v_base="-c:v libx264 -preset veryfast -keyint_min 100 -g 100 -sc_threshold 0 -r 25 -pix_fmt yuv420p -crf 18"
#v_worst="-b:v 72k -minrate 36k -maxrate 72k -bufsize 90k -s 128x72 -max_muxing_queue_size 1024"
v_bad="-b:v 144k -minrate 72k -maxrate 100k -bufsize 140k -s 256x144 -max_muxing_queue_size 1024"
v_low="-b:v 240k -minrate 144k -maxrate 240k -bufsize 380k -s 384x216 -max_muxing_queue_size 1024"
v_med="-b:v 360k -minrate 240k -maxrate 360k -bufsize 720k -s 640x360 -max_muxing_queue_size 1024"
v_high="-b:v 540k -minrate 360k -maxrate 540k -bufsize 1080k -s 896x504 -max_muxing_queue_size 1024"
v_best="-b:v 720k -minrate 540k -maxrate 720k -bufsize 1440k -s 1280x720 -max_muxing_queue_size 1024"
v_max="-b:v 720k -minrate 720k -maxrate 1440k -bufsize 2500k -s 1280x720 -max_muxing_queue_size 2048"

on_die()
{
    pkill -P $$ 2>>$LOGDIR/${SN}_transcode.log 1>>$LOGDIR/${SN}_transcode.log
}
trap 'on_die' EXIT

cmd="$FFMPEGSOURCE/ffmpeg -progress $PROGRESSFILE -hide_banner -loglevel error -nostdin $SOURCE_PARAMS \
$a_bad   $v_base0 $v_bad   $o_bad   \
$a_low   $v_base  $v_low   $o_low   \
$a_med   $v_base  $v_med   $o_med   \
$a_high  $v_base  $v_high  $o_high  \
$a_best  $v_base  $v_best  $o_best  \
$a_best  $v_base  $v_max   $o_max   \
$o_orig"

echo "$(date +%c) Starting transcode of $SN from $SOURCE with $cmd" >> $LOGDIR/${SN}_transcode.log

$cmd 2>>$LOGDIR/isha-monitoring.log &
wait

echo "$(date +%c) Finished transcode" >> $LOGDIR/${SN}_transcode.log
