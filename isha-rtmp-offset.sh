#!/bin/bash

# TODO: remember offset

# Streaming a local file to the local rtmp server for testing purposes
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$script_dir/isha-common.sh"

local_stream () {

	# What should we call the stream
	local source="$1"

	# Rtmp server address, youtube or localhost 
	local destination="$2"
	local stream_key="$3"
	local slog=$LOGDIR/${stream_key}_rtmp.log

	log_info "Starting playing from master file $source to $destination / $stream_key"
	# Todo: Remove dot from $source and get offset
	FFMPEG_OFFSET=
#local offset=$4	
#local offset=$(calculate_offset $source)
#	if [ "$offset" -gt "0" ]; then
#		echo "Going to sleep for $offset seconds"
#	  sleep $offset
    # Out of scope for now:
    # play_placeholder_video $offset
	  # -i placeholder image -t duration 
#	else
	#	local offset_inverse=$((offset * -1))
	  FFMPEG_OFFSET="-ss $4" 
#	fi

	touch $slog

	log_info "Starting to play source $source destination $destination stream key $stream_key"
	log_info "Waiting until file available"

	until [ -f $RECDIR/$source ]
	do
	     sleep 2
	     echo "rechecking..." >> $slog
	done

	log_info "Got file, now starting with $FFMPEGSOURCE/ffmpeg -y -hide_banner -loglevel error -nostdin -fflags +igndts  -re -i $source -c copy -fflags +genpts -f flv $destination/$stream_key"

	local time1=$(date +%c)
	echo "${time1} Starting" >> $slog

	PROGRESSFILE=$LOGDIR/${stream_key}_rtmp_progress.log
	$FFMPEGSOURCE/ffmpeg -y -hide_banner -loglevel error -progress $PROGRESSFILE -nostdin -fflags +igndts  -re -i $RECDIR/$source -c copy -fflags +genpts -f flv $destination/$stream_key 2>>$LOGDIR/isha-monitoring.log

	local time1=$(date +%c)
	echo "${time1} Exiting" >> $slog
}

on_die ()
{
	  log_info "Player stopped playing $1 to $3"
		
    # kill all children
    pkill -P $$
}
trap 'on_die' EXIT INT TERM

local_stream $1 $2 $3 ${4-} &
