#!/bin/bash

usage() { 
  # stream name: "fmf_stream2_session1"
  # 
    echo -e "usage: isha-vod \n\
    -s <stream name> \n\
    [-f <number of seconds to remove from start, default 2520=42 minutes>] \n\
    [-r (retranscode with GPU before packaging)]
    [-o <custom output filename>]
    [-e <source file extension, defaults to '.ts'>] \n\
    [-k (specify to keep intermediary files afterwards instead of deleting)] \n\
    [-d (directly package without cutting first, temp file needs to exist)]" >&2
  exit 1; 
}

function package_stream() {
    SN=$1
    FROM=$2
    RETRANSCODE=$3
    KEEP_TEMP_FILES=$4
    SRC_EXT=$5
    DIRECTLY_PACKAGE=$6
    OUTPUT_FILENAME=$7

    FFMPEG_FROM="-ss $FROM"

    # PSEUDOCODE: cut the video

    EXT1="_bad"
    EXT2="_low"
    EXT3="_med"
    EXT4="_high"
    EXT5="_best"
    EXT6="_max"

    PROGRESSFILE=$LOGDIR/${SN}_vod_progress.log
    LOGFILE=$LOGDIR/${SN}_vod.log

    on_die()
    {
        pkill -P $$ 2>>$LOGDIR/${SN}_transcode.log 1>>$LOGDIR/${SN}_transcode.log
        exit 1
    }
    trap 'on_die' EXIT SIGINT

    cmd="$FFMPEGSOURCE/ffmpeg -y -hide_banner -loglevel error -nostdin -progress $PROGRESSFILE $FFMPEG_FROM -i $RECDIR/${SN}${EXT1}${SRC_EXT} $FFMPEG_FROM -i $RECDIR/${SN}${EXT2}${SRC_EXT} $FFMPEG_FROM -i $RECDIR/${SN}${EXT3}${SRC_EXT} $FFMPEG_FROM -i $RECDIR/${SN}${EXT4}${SRC_EXT} $FFMPEG_FROM -i $RECDIR/${SN}${EXT5}${SRC_EXT} $FFMPEG_FROM -i $RECDIR/${SN}${EXT6}${SRC_EXT} \
    -map 0:v -map 0:a -c copy -f mp4 ${RECDIR}/${SN}${EXT1}_cut${FROM}.mp4 \
    -map 1:v -map 1:a -c copy -f mp4 ${RECDIR}/${SN}${EXT2}_cut${FROM}.mp4 \
    -map 2:v -map 2:a -c copy -f mp4 ${RECDIR}/${SN}${EXT3}_cut${FROM}.mp4 \
    -map 3:v -map 3:a -c copy -f mp4 ${RECDIR}/${SN}${EXT4}_cut${FROM}.mp4 \
    -map 4:v -map 4:a -c copy -f mp4 ${RECDIR}/${SN}${EXT5}_cut${FROM}.mp4 \
    -map 5:v -map 5:a -c copy -f mp4 ${RECDIR}/${SN}${EXT6}_cut${FROM}.mp4"

    IN1=${RECDIR}/${SN}${EXT1}_cut${FROM}.mp4
    IN2=${RECDIR}/${SN}${EXT2}_cut${FROM}.mp4
    IN3=${RECDIR}/${SN}${EXT3}_cut${FROM}.mp4
    IN4=${RECDIR}/${SN}${EXT4}_cut${FROM}.mp4
    IN5=${RECDIR}/${SN}${EXT5}_cut${FROM}.mp4
    IN6=${RECDIR}/${SN}${EXT6}_cut${FROM}.mp4

    if [ ${RETRANSCODE} -eq 1 ]; then
        IN1=${RECDIR}/${SN}${EXT1}_cut${FROM}_retransc.mp4
        IN2=${RECDIR}/${SN}${EXT2}_cut${FROM}_retransc.mp4
        IN3=${RECDIR}/${SN}${EXT3}_cut${FROM}_retransc.mp4
        IN4=${RECDIR}/${SN}${EXT4}_cut${FROM}_retransc.mp4
        IN5=${RECDIR}/${SN}${EXT5}_cut${FROM}_retransc.mp4
        IN6=${RECDIR}/${SN}${EXT6}_cut${FROM}_retransc.mp4


        exit 0
    fi
    echo "dir pak is $DIRECTLY_PACKAGE"
    if [ ${DIRECTLY_PACKAGE} -ne 1 ]; then
        echo "Starting VOD packaging for ${SN}: cutting each video for $FROM s removed from the start, command $cmd"
        echo "Starting VOD packaging for ${SN}: cutting each video for $FROM s removed from the start, command $cmd" >> $LOGFILE
        chmod 777 $LOGFILE
        log_info "Starting VOD packaging for ${SN}: cutting each video for $FROM s removed from the start"

        $cmd
    fi

    # THEN RUN transcode
    cmd="$PACKAGERSOURCE \
    in=${IN1},stream=audio,init_segment=${OUTPUT_FILENAME}${EXT1}_audio/init.mp4,playlist_name=${OUTPUT_FILENAME}${EXT1}_audio.m3u8,segment_template=${OUTPUT_FILENAME}${EXT1}_audio/"'$Number$'".m4s \
    in=${IN2},stream=audio,init_segment=${OUTPUT_FILENAME}${EXT2}_audio/init.mp4,playlist_name=${OUTPUT_FILENAME}${EXT2}_audio.m3u8,segment_template=${OUTPUT_FILENAME}${EXT2}_audio/"'$Number$'".m4s \
    in=${IN3},stream=audio,init_segment=${OUTPUT_FILENAME}${EXT3}_audio/init.mp4,playlist_name=${OUTPUT_FILENAME}${EXT3}_audio.m3u8,segment_template=${OUTPUT_FILENAME}${EXT3}_audio/"'$Number$'".m4s \
    in=${IN4},stream=audio,init_segment=${OUTPUT_FILENAME}${EXT4}_audio/init.mp4,playlist_name=${OUTPUT_FILENAME}${EXT4}_audio.m3u8,segment_template=${OUTPUT_FILENAME}${EXT4}_audio/"'$Number$'".m4s \
    in=${IN5},stream=audio,init_segment=${OUTPUT_FILENAME}${EXT5}_audio/init.mp4,playlist_name=${OUTPUT_FILENAME}${EXT5}_audio.m3u8,segment_template=${OUTPUT_FILENAME}${EXT5}_audio/"'$Number$'".m4s \
    in=${IN6},stream=audio,init_segment=${OUTPUT_FILENAME}${EXT6}_audio/init.mp4,playlist_name=${OUTPUT_FILENAME}${EXT6}_audio.m3u8,segment_template=${OUTPUT_FILENAME}${EXT6}_audio/"'$Number$'".m4s \
    in=${IN1},stream=video,init_segment=${OUTPUT_FILENAME}${EXT1}/init.mp4,playlist_name=${OUTPUT_FILENAME}${EXT1}.m3u8,segment_template=${OUTPUT_FILENAME}${EXT1}/"'$Number$'".m4s \
    in=${IN2},stream=video,init_segment=${OUTPUT_FILENAME}${EXT2}/init.mp4,playlist_name=${OUTPUT_FILENAME}${EXT2}.m3u8,segment_template=${OUTPUT_FILENAME}${EXT2}/"'$Number$'".m4s \
    in=${IN3},stream=video,init_segment=${OUTPUT_FILENAME}${EXT3}/init.mp4,playlist_name=${OUTPUT_FILENAME}${EXT3}.m3u8,segment_template=${OUTPUT_FILENAME}${EXT3}/"'$Number$'".m4s \
    in=${IN4},stream=video,init_segment=${OUTPUT_FILENAME}${EXT4}/init.mp4,playlist_name=${OUTPUT_FILENAME}${EXT4}.m3u8,segment_template=${OUTPUT_FILENAME}${EXT4}/"'$Number$'".m4s \
    in=${IN5},stream=video,init_segment=${OUTPUT_FILENAME}${EXT5}/init.mp4,playlist_name=${OUTPUT_FILENAME}${EXT5}.m3u8,segment_template=${OUTPUT_FILENAME}${EXT5}/"'$Number$'".m4s \
    in=${IN6},stream=video,init_segment=${OUTPUT_FILENAME}${EXT6}/init.mp4,playlist_name=${OUTPUT_FILENAME}${EXT6}.m3u8,segment_template=${OUTPUT_FILENAME}${EXT6}/"'$Number$'".m4s \
    --generate_static_live_mpd --mpd_output ${OUTPUT_FILENAME}.mpd \
    --hls_playlist_type VOD --hls_master_playlist_output ${OUTPUT_FILENAME}_master.m3u8"

    log_info "VOD packaging second step for ${SN}"
    echo -e "\n$(date +%c) VOD packaging second step for ${SN}: $cmd"
    echo -e "\n$(date +%c) VOD packaging second step: ${SN}: $cmd" >> $PROGRESSFILE
    
    packager_output=`echo $( cd ${RECDIR}/vod && $cmd 2>>$LOGDIR/isha-monitoring.log ) &`
    wait

    echo "$(date +%c) Packager output: $packager_output"
    if [ "$packager_output" == "Packaging completed successfully." ]; then
        echo " Successfully packaged ${SN}"
        echo " Successfully packaged ${SN}" >> $LOGFILE
    else
        handle_error "Had a problem packaging $SN: $packager_output"
    fi

    if [ "${KEEP_TEMP_FILES}" -ne 1 ]; then
        rm "${RECDIR}/${SN}${EXT1}_cut${FROM}.mp4"
        rm "${RECDIR}/${SN}${EXT2}_cut${FROM}.mp4"
        rm "${RECDIR}/${SN}${EXT3}_cut${FROM}.mp4"
        rm "${RECDIR}/${SN}${EXT4}_cut${FROM}.mp4"
        rm "${RECDIR}/${SN}${EXT5}_cut${FROM}.mp4"
        rm "${RECDIR}/${SN}${EXT6}_cut${FROM}.mp4"
    fi
}


# Autoexecute if being called directly
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]
then
    script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    source "$script_dir/isha-common.sh" || true

    # Defaults
    FROM=2520
    SRC_EXT=.ts
    KEEP_TEMP_FILES=0
    DIRECTLY_PACKAGE=0
    RETRANSCODE=0

    while getopts ":s:f:o:kre:d" o; do
        case "${o}" in
            s)
                SN=${OPTARG}
                ;;
            f)
                FROM=${OPTARG}
                ;;
            r)
                RETRANSCODE=1
                ;;
            k)
                KEEP_TEMP_FILES=1
                ;;
            e)
                SRC_EXT=${OPTARG}
                ;;
            o)
                OUTPUT_FILENAME=${OPTARG}
                ;;
            d)
                DIRECTLY_PACKAGE=1
                ;;
            *)
                echo "Unknown param: $o"
                usage
                ;;  
        esac
    done
    shift $((OPTIND-1))

    if [ -z "${SN}" ]; then
        usage
    fi

    if [ -z $OUTPUT_FILENAME ]; then
        OUTPUT_FILENAME=${SN}
    fi

    echo "dir pak is $DIRECTLY_PACKAGE"

    package_stream $SN $FROM $RETRANSCODE $KEEP_TEMP_FILES $SRC_EXT $DIRECTLY_PACKAGE $OUTPUT_FILENAME
fi
