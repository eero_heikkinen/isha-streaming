#!/bin/bash

#if [ -n "${ISHA_STREAMING-}" ]; then return; fi; 
#ISHA_STREAMING=0; # pragma once

# Include common functions
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${script_dir}/isha-config.sh"
source "${script_dir}/isha-monitoring.sh"
for program in ${script_dir}/sessions/*.sh; do
  source $program; 
  returncode=$?
  if [[ $returncode -ne 0 ]]; then
    handle_error "Error while parsing entries for $program"
  fi
done

[ "${BASH_VERSINFO:-0}" -lt 4 ] && handle_error "Invoked with outdated bash version: $BASH_VERSINFO"

function get_session_value() {
  SESSION_ID=$1
  KEY=$2
  varname=TIMINGS_${SESSION_ID}[$KEY]
  VALUE=${!varname:-}  
  echo $VALUE
}

function get_session_timestamp() {
  SESSION_NAME=$1
  KEY=$2

  TIMESTAMP_STRING=$(get_session_value $SESSION_NAME $KEY)
  if [[ -z $TIMESTAMP_STRING ]]; then
    handle_error "Could not find section $KEY for $SESSION_NAME"
    echo 0
    return 1
  fi

  TIMESTAMP_INT=$(iso8601_to_unix_timestamp $TIMESTAMP_STRING)
  echo $TIMESTAMP_INT
}

function remove_language() {
  # make lowercase
  local sn=${1,,}

  if [[ $sn =~ _stream[0-9]{1,2}_ ]]; then
    re='+([0-9])'
    without_language=$(echo $sn | sed "s/_stream[0-9]\{1,2\}//")
    echo $without_language
  else
    echo $sn
  fi
}

function remove_variant() {
  # make lowercase
  local sn=${1,,}

  if [[ $sn =~ _(worst|bad|low|med|high|best|max)$ ]]; then
    stream_id=${sn/${BASH_REMATCH[0]}/}
    echo $stream_id
  else 
    echo $sn
  fi
}

function session_exists() {
  local session_id=$1

  if [[ "$(declare -p TIMINGS_$session_id 2>/dev/null)" == "declare -A"* ]]; then
   return 0
  else
   return -1
  fi
}

function has_non_t1_timezone() {
  local sn=$1
  if [[ $sn =~ _t[2-9][0-9]?_ ]]; then
    return 0
  else
    return -1
  fi
}

function parse_sdata() {
  local sn=$1
  local session_id=$(remove_language $(remove_variant $sn))

  if ! session_exists $session_id; then
    return 1
  fi
 
  declare -gA sdata=(
   ["stream_start"]=$(get_session_timestamp $session_id stream_start)
   ["session_start"]=$(get_session_timestamp $session_id session_start)
   ["session_end"]=$(get_session_timestamp $session_id session_end)
   ["stream_end"]=$(get_session_timestamp $session_id stream_end)
  )

  local delayed=$(get_session_value $session_id delayed)
  if [[ -z $delayed ]]; then
    if has_non_t1_timezone $sn; then
      sdata["delayed"]="true"
    else
      sdata["delayed"]="false"
    fi
  else
    sdata["delayed"]=$delayed
  fi

  if [[ -z ${sdata["stream_start"]} ]]; then
    handle_error "Missing stream_start for $session_id"
  fi
  if [[ -z ${sdata["session_start"]} ]]; then
    handle_error "Missing session_start for $session_id"
  fi
  if [[ -z ${sdata["session_end"]} ]]; then
    handle_error "Missing session_end for $session_id"
  fi
  if [[ -z ${sdata["stream_end"]} ]]; then
    handle_error "Missing stream_end for $session_id"
  fi

  if [[ ! ${sdata["stream_start"]} -le ${sdata["session_start"]} ]] \
    || [[ ! ${sdata["session_start"]} -le ${sdata["session_end"]} ]] \
    || [[ ! ${sdata["session_end"]} -le ${sdata["stream_end"]} ]]; then
      handle_error "Misconfigured date for $session_id: $(declare -p sdata)"
  fi

  local now=$(timestamp_now)
  if   [[ $now -lt ${sdata["stream_start"]} ]]; then
    sdata["status"]=test
  elif [[ $now -lt ${sdata["session_start"]} ]]; then
    sdata["status"]=before_session_start
  elif [[ $now -lt ${sdata["session_end"]} ]]; then
    sdata["status"]=session
  elif [[ $now -lt ${sdata["stream_end"]} ]]; then
    sdata["status"]=after_session_end
  else 
    sdata["status"]=test
  fi

  # Parse streams (languages)
  declare -ga sdata_streams
  # split by comma into temp array
  IFS=',' read -r -a singular_stream_names <<< "$(get_session_value $session_id streams)"
  for singular_stream_name in "${singular_stream_names[@]}"; do
    stream_name=$(populate_session_id_from_language_string $session_id $singular_stream_name)
    sdata_streams+=($stream_name)
  done
}

function read_progress_stats() {
  local file=$1

  FRAME=
  FPS=
  BITRATE=
  TOTAL_SIZE=
  OUT_TIME_MS=
  OUT_TIME=
  DUP_FRAMES=
  DROP_FRAMES=
  PROGRESS=
  SPEED=
  
  if [ ! -e "$file" ]; then
    # File does not exist
    PROGRESS_UPDATED=-1
    return
  fi 

  PROGRESS_UPDATED=$(($($DATE +%s) - $($DATE +%s -r $file)))

  while IFS="=" read -r key value; do
    case "$key" in
      "frame")        FRAME="$value" ;;
      "fps")          FPS="$value" ;;
      "bitrate")      BITRATE="$value" ;;
      "total_size")   TOTAL_SIZE="$value" ;;
      "out_time_ms")  OUT_TIME_MS="$value" ;;
      "out_time")     OUT_TIME="$value" ;;
      "dup_frames")   DUP_FRAMES="$value" ;;
      "drop_frames")  DROP_FRAMES="$value" ;;
      "progress")     PROGRESS="$value" ;;
      "speed")        SPEED="$value" ;;
    esac
  done < "$file"
}

function record_progress_filename() {
  local stream_id=$1
  echo $LOGDIR/${stream_id}_record_progress.log
}

function transcode_progress_filename() {
  local stream_id=$1
  echo $LOGDIR/${stream_id}_transcode_progress.log
}

function vbr_progress_filename() {
  local stream_id=$1
  echo $LOGDIR/${stream_id}_player_progress.log
}

function placeholder_pid_filename() {
  local stream_id=$1
  echo $PIDDIR/placeholder_${stream_id}.pid
}

function player_pid_filename() {
  local stream_id=$1
  echo $PIDDIR/player_${stream_id}.pid
}

# in: my_test_sn rtmp://localhost/receive
# out: $PIDDIR/my_test_sn.localhost_receive.lock
# 
# in: my_test_sn rtmps://a.rtmps.youtube.com/live2
# out: $PIDDIR/my_test_sn.a_rtmps_youtube_com_live2.lock
function lockfile_for_sn_and_dest() {
  local sn=$1
  local dest=$2

  if [[ "$dest"  == rtmp://* ]]; then
    # remove rtmp://
    key=${dest/rtmp:\/\//}
  elif [[ "$dest"  == rtmps://* ]]; then
    # remove rtmps://
    key=${dest/rtmps:\/\//}
  else
    msg="dest url \"$dest\" does not begin with rtmp:// or rtmps://"
    echo "$msg" && handle_error "$msg"
    return 1
  fi

  # clean out :1935
  key=${key/:1935/\/}

  # clean out any double forward slashes (eg. localhost/receive//stream_name )
  key=${key/\/\//\/}

  # replace dots and forward slashes with underscores
  key=${key//\//_}
  key=${key//./_}

  echo "$PIDDIR/${sn}.${key}.lock"
}

function acquire_lock_or_fail() {
  local lockfile=$1

  exec 4<>$lockfile || { 
    msg="Fatal: error when running 'exec 4<>$lockfile'"
    handle_error "$msg" && echo "$msg"
    exit 1
  }
  flock -n 4 || { 
    msg="Fatal: Something else is already streaming with this stream name. Lock: $lockfile"
    handle_error "$msg" && echo "$msg"
    exit 1
  }
}

ensure_pidfile_cleared_or_fail() {
  local pidfile=$1
  if [[ -e $pidfile ]]; then 
    # the file exists
    
    existing_pid=$(cat $pidfile)
    if ps -p $existing_pid > /dev/null; then
      # referenced process still running, attempt to shutdown
      
      kill $existing_pid
      sleep 5
      if ps -p $existing_pid > /dev/null; then
        # could not shutdown, let's try to force
         
        handle_error "Could not kill process $pid from $pidfile within 5 seconds using SIGTERM "
        
        pkill -P $existing_pid
        kill -9 $existing_pid
        sleep 1

        if ps -p $existing_pid > /dev/null; then
          handle_error "Could not kill process $pid from $pidfile even using -9"
          exit 1
        fi
      fi
    else
      # referenced process no longer exists
      
      handle_error "Pidfile $pidfile is stale. Removing"
      rm $pidfile
    fi
  fi
}

function clear_progress_files() {
  local stream_id=$1

  RECORD_PROGRESS_FILE=$LOGDIR/${stream_id}_record_progress.log
  TRANSCODE_PROGRESS_FILE=$LOGDIR/${stream_id}_transcode_progress.log

  echo "" > $RECORD_PROGRESS_FILE
  echo "" > $TRANSCODE_PROGRESS_FILE
}

function parent_id_from_delayed_stream_id() {
  echo "unimplemented"
}

function current_offset_from_section() {
  local stream_id=$1
  local section=$2
  local now=$(timestamp_now)

  parse_sdata $stream_id
  if [[ -z ${sdata[$section]-} ]]; then
    handle_error "MISSING $section FROM $stream_id !" 
    return 1
  fi

  echo $(($now-${sdata[$section]}))
}

function populate_session_id() {
  local session_id=$1
  local stream_number=$2
  echo $session_id | sed "s/_session/_stream${stream_number}_session/"
}

function populate_session_id_from_language_string() {
  local session_id=$1
  local language_string=$2
  echo $session_id | sed "s/_session/_${language_string}_session/"
}

function ongoing_sessions() {
  for session_timings_variable in ${!TIMINGS_*}
  do
      local session_id=${session_timings_variable#"TIMINGS_"}

      parse_sdata $session_id

      now=$(timestamp_now)
      if [[ $now -ge ${sdata[stream_start]} ]] && [[ $now -lt ${sdata[stream_end]} ]]; then
        echo $session_id
      fi
  done
}

function video_length_from_filename() {
  local file=$1
  LENGTH=$($FFMPEGSOURCE/ffprobe -i $file -show_entries format=duration -v quiet -of csv="p=0")
  if [ -z "$LENGTH" ] || [ $LENGTH == 'N/A' ]
  then
    # Nothing recorded for this session
    echo "0.0"
  else
    echo $LENGTH
  fi
}

function calculate_offset() {
  local stream_id=$1
  parse_sdata $session_id

  log_info "Calculate offset with $stream_id"

  parse_sdata $SN

  if [[ -z ${sdata[@]-} ]]; then
    echo 0
    log_info "no session"
    return
  fi
  log_info "session exist"

  # Get time period from earliest possible recording start
  # to start of session
  
  local T1=$(unix_timestamp_to_date_india ${sdata[stream_start]})
  local T2=$(unix_timestamp_to_date_india ${sdata[session_start]})
  echo "Times: $T1 $T2" >>$LOGDIR/isha-monitoring.log
  # Filter log file for relevant events

  EVENTS=$(awk -v string1="$T1" -v string2="$T2" '$1" "$2>=string1 && $1" "$2<=string2' $LOGDIR/isha-monitoring.log | grep -e "started recording" -e "resumed recording" -e "stopped recording" | grep $stream_id)
  # Explicitly declare as int
  declare -i OFFSET=0

  # Iterate over lines referencing stopping and starting
  
  while IFS= read -r line; do
    ITEMS=($line)
    TIMESTAMP=`date_to_unix_timestamp_india ${ITEMS[0]} ${ITEMS[1]}`

    if [[ $line =~ "started recording" ]]; then
      # It's the first time stream has started; find the referenced offset from scheduled time
      REGEX='offset:(-?[0-9]+) s '
      if [[ $line =~ $REGEX ]] ; then
        OFFSET=${BASH_REMATCH[1]}
      fi
    elif [[ $line =~ "resumed recording" ]]; then
      if [[ -z $LAST_STOPPED ]]; then
        handle_error "resumed recording without previous recording end found"
        echo 0
        return
      fi
      # Not the first item anymore
      NEW_OFFSET=$((TIMESTAMP-LAST_STOPPED))
      if [[ "$NEW_OFFSET" -lt "0" ]] ; then
        # Todo handle error. New offset should never be < 0
        NEW_OFFSET=0
        handle_error "New offset negative for $stream_id on line $line"
      fi
      OFFSET=$((OFFSET+NEW_OFFSET))
      # Clear temp variable since restarted recording
      LAST_STOPPED=
    elif [[ $line =~ "stopped recording" ]]; then
      LAST_STOPPED=$TIMESTAMP
    fi
  done <<< "$EVENTS"

  if [[ "$OFFSET" -gt "3600" ]] || [[ "$OFFSET" -lt "-3600" ]]; then
    handle_error "Strange offset: $OFFSET for $stream_id. Reverting to 0."
    OFFSET=0
  fi

  echo "$OFFSET"
}

# Timestamp related

function timestamp_now() {
  echo `$DATE +%s`
}

function iso8601_to_unix_timestamp() {
  echo `$DATE -d $1 +%s`
}

function unix_timestamp_to_date_india() {
  echo `TZ="Asia/Kolkata" $DATE "+%Y-%m-%d %H:%M:%S" -d "@$1"`
}

function date_to_unix_timestamp_india() {
  echo `TZ="Asia/Kolkata" $DATE +'%s' -d "$1 $2"`
}

test_dash() {
  SN=$1
  FILE=$DASHDIR/$SN.mpd
  if [ -e "$FILE" ]; then
    SECONDS_SINCE_LAST_EDIT=$(($($DATE +%s) - $($DATE +%s -r $FILE)))
    echo $SECONDS_SINCE_LAST_EDIT
  else
    echo "N/A"
  fi 
}

process_dash_status() {
  SN=$1
  TOLERANCE=${2:-5}
  UPDATE_TIME=$(test_dash $SN)

  if [[ $UPDATE_TIME == 'N/A' ]]; then
    echo "missing"
  elif [[ $UPDATE_TIME -ge 0 ]] && [[ $UPDATE_TIME -lt $TOLERANCE ]]; then
    echo "ok"
  else
    echo "stale"
  fi
}

player_active() {
  SN=$1
  PIDFILE=$(player_pid_filename $SN)
  if [[ -e $PIDFILE ]] && ps -p $(cat $PIDFILE) > /dev/null; then
    # true
    return 0
  else
    # false
    return 1
  fi
}

test_hls() {
  SN=$1
  FILE=$HLSDIR/$SN.m3u8
  if [ -e "$FILE" ]; then
      SECONDS_SINCE_LAST_EDIT=$(($($DATE +%s) - $($DATE +%s -r $FILE)))
      echo $SECONDS_SINCE_LAST_EDIT
  else
      echo "N/A"
  fi 
}

process_hls_status() {
  SN=$1
  TOLERANCE=${2:-5}
  UPDATE_TIME=$(test_hls $SN)

  if [[ $UPDATE_TIME == 'N/A' ]]; then
    echo "missing"
  elif [[ $UPDATE_TIME -ge 0 ]] && [[ $UPDATE_TIME -lt $TOLERANCE ]]; then
    echo "ok"
  else
    echo "stale"
  fi
}

function validate_config() {
  if [ ! -d $LOGDIR ]; then handle_error "Directory does not exist: $LOGDIR"; fi
  if [ ! -d $RECDIR ]; then handle_error "Directory does not exist: $RECDIR"; fi
  if [ ! -d $DASHDIR ]; then handle_error "Directory does not exist: $DASHDIR"; fi
  if [ ! -d $HLSDIR ]; then handle_error "Directory does not exist: $HLSDIR"; fi
  if [ ! -d $FFMPEGSOURCE ]; then handle_error "Directory does not exist: $FFMPEGSOURCE"; fi

  for filename in $LOGDIR/* ; do
      if [ ! -w $filename ]; then 
          log_info "Non writable file in log directory: $filename" 
      fi 
  done 

  # This will parse and validate all defined sessions
  ongoing_sessions
}