#!/bin/bash

#export SENTRY_DSN='https://0dc68ed47c5e45ccb45c1ef67932a9da@o540178.ingest.sentry.io/5658210'
#eval "$(sentry-cli bash-hook)"

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$script_dir/isha-config.sh"

LOGFILE=$LOGDIR/isha-monitoring.log

function timestamp() {
  echo `TZ="Asia/Kolkata" $DATE "+%Y-%m-%d %H:%M:%S"`
}

function session_starting() {
  stream_id=$1
  countdown=$2

  if [ "$countdown" -ge "270" ] && [ "$countdown" -le "330" ] ; then
    echo "$(timestamp) 🔔 $stream_id about to start streaming in 5 minutes" >> $LOGFILE
  elif [ "$countdown" -lt "90" ] && [ "$countdown" -ge "30" ]; then
    echo "$(timestamp) 🔔 $stream_id about to start streaming in 1 minute" >> $LOGFILE
  elif [ "$countdown" -lt "30" ] && [ "$countdown" -ge "-5" ]; then 
    echo "$(timestamp) 🔔 $stream_id streaming scheduled to start now" >> $LOGFILE
  fi
}

function recording_started() {
  local stream_id=$1
  local previous_recorded_length=${2:-0}

  parse_sdata $SN

  if [[ -z ${sdata[@]-} ]]; then
   echo "$(timestamp) ⚠️  $stream_id started recording (unknown stream id)" >> $LOGFILE
  else
    local offset=$(current_offset_from_section $stream_id stream_start) || 0

    if (( $(echo "$previous_recorded_length > 0.0" | bc -l) )); then
      echo "$(timestamp) ⚠️  $stream_id resumed recording with previous length ${previous_recorded_length} s" >> $LOGFILE
    elif [ "$offset" -lt "60" ]; then
      echo "$(timestamp) ✅ $stream_id started recording with offset:$offset s from scheduled start" >> $LOGFILE
    else
      echo "$(timestamp)    $stream_id started recording with offset:$offset s from scheduled start" >> $LOGFILE
    fi
  fi
}

# Recording stopped

function recording_stopped() {
  local TIMESTAMP=$(TZ="Asia/Kolkata" date "+%Y-%m-%d %H:%M:%S")
  local stream_id=$1

  #local offset_session_start=$(current_offset_from_section $stream_id session_start) || 0
  #local offset_session_end=$(current_offset_from_section $stream_id session_end) || 0
  #local offset_stream_end=$(current_offset_from_section $stream_id stream_end) || -1

  echo "$(timestamp) ⚠️  $stream_id stopped recording" >> $LOGFILE
  
  # log_info "offset see start $offset_session_start"
  # if [ ${offset_session_start:-0} -lt 0 ]; then
  #   # Stopped, session not yet started
  #   echo "$(timestamp) ⚠️  $stream_id stopped recording" >> $LOGFILE
  # elif [ ${offset_session_end:-0} -lt 0 ]; then
  #   # Stopped, in middle of session
  #   echo "$(timestamp) ❗️ $stream_id stopped recording in middle of session" >> $LOGFILE
  # elif [ ${offset_stream_end:-1} -lt 0 ]; then
  #   echo "$(timestamp) ⚠️  $stream_id stopped recording before stream end" >> $LOGFILE
  # elif [ ${offset_stream_end:-1} -ge 0 ]; then
  #   echo "$(timestamp) ✅ $stream_id stopped recording as scheduled" >> $LOGFILE
  # else 
  #   echo "$(timestamp) ❗️ $stream_id stopped recording with unknown offset:${offset_stream_end}" >> $LOGFILE
  # fi
}

# Recording should be on according to schedule
# but master stream is not recording

function recording_offline() {
  SN=$1
  echo "$(timestamp) ❗️ $SN NOT RECORDING" >> $LOGFILE
}

# Status ticker for stream

function stream_status() {
  SN=$1
  RECORD_BITRATE=$2
  RECORD_FPS=$3
  TRANSCODE_BITRATE=$4
  TRANSCODE_FPS=$5
  DASH_STATUS=$6
  HLS_STATUS=$7

  pieces=(`parse_stream_id $SN`)
  stream_name=${pieces[2]}

  if [ -z $TRANSCODE_BITRATE ] || [ $TRANSCODE_BITRATE == 'N/A' ]; then
    echo "$(timestamp)     $stream_name ❌ Offline ($SN)" >> $LOGFILE  
    return
  fi

  RECORD_STATUS="recording @ $RECORD_BITRATE $RECORD_FPS fps"

  if [ "$DASH_STATUS" -lt "10" ] 2>/dev/null
  then
    DASH_STATUS="refreshed ${DASH_STATUS}s ago ✅ "
  else
    DASH_STATUS="refreshed ${DASH_STATUS}s ago ❗️ "
  fi

  if [ "$HLS_STATUS" != 'N/A' ] && [ "$HLS_STATUS" -lt "10" ]; then
    HLS_STATUS="refreshed ${HLS_STATUS}s ago ✅ "
  else
    HLS_STATUS="refreshed ${HLS_STATUS}s ago ❗️ "
  fi

  echo "$(timestamp)     $stream_name ✅ Online ($SN)
                         $RECORD_STATUS 
                         transcoding @ $TRANSCODE_BITRATE $TRANSCODE_FPS fps
                         DASH $DASH_STATUS HLS $HLS_STATUS" >> $LOGFILE       
}

function dash_offline() {
  SN=$1
  echo "$(timestamp) ❗️ $SN DASH STREAM OFFLINE" >> $LOGFILE
}

function hls_offline() {
  SN=$1
  echo "$(timestamp) ❗️ $SN HLS STREAM OFFLINE" >> $LOGFILE
}

# nginx RTMP server not reachable!

function nginx_offline() {
  SN=$1
  echo "$(timestamp) ❗️❗️❗️❗️❗️ Nginx offline❗️❗️❗️❗️❗️" >> $LOGFILE
}

function handle_error() {
  if [ -f $LOGFILE ]; then
    echo "$(timestamp) ❗️ Error: $1" >> $LOGFILE
    #sentry-cli send-event -m "$1" --logfile $LOGFILE >>/dev/null 2>>/dev/null
  else
    echo "$(timestamp) ❗️ Logfile misconfigured"
    echo "$(timestamp) ❗️ Error: $1"
    #sentry-cli send-event -m "$1"
  fi
}

function log_info() {
  echo "$(timestamp)    $1" >> $LOGFILE
}

