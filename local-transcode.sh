APARAM0="-map 0:a -c:a copy"
VPARAM0="-map 0:v -c:v copy"
RPARAM0="-max_muxing_queue_size 1024 -f mpegts - "

# Changed libfdk_aac -> aac for osx test. changed back - sharad
a_worst="-c:a libfdk_aac -ac 2 -b:a 32k"
a_bad="-c:a libfdk_aac -ac 2 -b:a 32k"
a_low="-c:a libfdk_aac -ac 2 -b:a 48k"
a_med="-c:a libfdk_aac -ac 2 -b:a 64k"
a_high="-c:a libfdk_aac -ac 2 -b:a 96k"
a_best="-c:a libfdk_aac -ac 2 -b:a 128k"

v_basew="-c:v libx264 -preset ultrafast -profile:v baseline -tune zerolatency -keyint_min 60 -g 60 -sc_threshold 0 -r 15 -pix_fmt yuv420p"
v_base0="-c:v libx264 -preset veryfast -profile:v baseline -tune zerolatency -keyint_min 60 -g 60 -sc_threshold 0 -r 15 -pix_fmt yuv420p -crf 18"
v_base="-c:v libx264 -preset veryfast -keyint_min 100 -g 100 -sc_threshold 0 -r 25 -pix_fmt yuv420p -crf 18"
#v_worst="-b:v 72k -minrate 36k -maxrate 72k -bufsize 90k -s 256x144 -max_muxing_queue_size 1024"
v_worst="-b:v 72k -minrate 36k -maxrate 72k -bufsize 90k -s 128x72 -max_muxing_queue_size 1024"
v_bad="-b:v 144k -minrate 72k -maxrate 100k -bufsize 140k -s 256x144 -max_muxing_queue_size 1024"
v_low="-b:v 240k -minrate 144k -maxrate 240k -bufsize 380k -s 384x216 -max_muxing_queue_size 1024"
v_med="-b:v 360k -minrate 240k -maxrate 360k -bufsize 720k -s 640x360 -max_muxing_queue_size 1024"
v_high="-b:v 540k -minrate 360k -maxrate 540k -bufsize 1080k -s 896x504 -max_muxing_queue_size 1024"
v_best="-b:v 720k -minrate 540k -maxrate 720k -bufsize 1440k -s 1280x720 -max_muxing_queue_size 1024"
v_max="-b:v 720k -minrate 720k -maxrate 1440k -bufsize 2500k -s 1280x720 -max_muxing_queue_size 2048"

# Transcode and forward to destination app
# Note: The first stream will form the basis for the progress statistics (bitrate etc)

/home/sharad_a/ffmpeg/ffmpeg -hide_banner -loglevel error -nostdin -i $1 \
$a_best  $v_base  $v_best  -f flv ${2}_best.ts  \
$a_worst $v_base0 $v_bad   -f flv ${2}_bad.ts   \
$a_low   $v_base  $v_low   -f flv ${2}_low.ts   \
$a_med   $v_base  $v_med   -f flv ${2}_med.ts   \
$a_high  $v_base  $v_high  -f flv ${2}_high.ts  \
$a_best  $v_base  $v_max   -f flv ${2}_max.ts  
