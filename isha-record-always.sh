#!/bin/bash

# Don't stop on errors.. prefer to attempt recording even with errors
set +e
set +o nounset

set -o pipefail

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$script_dir/isha-common.sh" || true

log_info "Connected to record with $1 $2 $3"
SN=$1 #stream name eg. program_tz_languageX_sessionY
APP=$2 # this is the rtmp application to record from eg. "distribute"
loglevel=${3:-} # if loglevel is "silent" skip some logging messages

SOURCE=rtmp://localhost:1935/$APP/$SN
RECFILE=$RECDIR/${SN}.ts
PROGRESSFILE=$LOGDIR/${SN}_record_progress.log
slog=$LOGDIR/${SN}_record.log


parse_sdata $SN

if [[ -z ${sdata[@]-} ]]; then
 ! log_info "Unrecognized session $SN - Recording"
else
	# There is a recognized session
	if [[ -n ${sdata["delayed"]-} ]] && [[ ${sdata["delayed"]} == 'true' ]]; then
		! log_info "Delayed stream $SN - sleeping"
		sleep 60
		exit 0
	fi
fi

on_die ()
{
	  echo "on die"
		#if [ "$loglevel" != 'silent' ]; then
			! recording_stopped $SN
		#fi
    # kill all children
    pkill -P $$ 2>>$slog 1>>$slog
}
trap 'on_die' EXIT INT TERM

echo "Detecting number of seconds for $SN" >> $slog
CURRENT_RECORDED_SECONDS=$(video_length_from_filename $RECFILE || 0)
echo "$SN with $CURRENT_RECORDED_SECONDS recorded" >> $slog

APPENDPARAM=
if (( $(echo "$CURRENT_RECORDED_SECONDS > 0.0" | bc -l) )); then
	APPENDPARAM="-copyts -output_ts_offset $CURRENT_RECORDED_SECONDS"
	echo "Resuming previous stream with length of $CURRENT_RECORDED_SECONDS seconds" >> $slog
fi

if [ "$loglevel" != 'silent' ]; then
	! recording_started $SN $CURRENT_RECORDED_SECONDS || true
fi

APARAM0="-map 0:a -c:a copy"
VPARAM0="-map 0:v -c:v copy"
RPARAM0="-max_muxing_queue_size 1024 -f mpegts -"

echo "Recording from /$APP/$SN to $RECFILE.. progress in $PROGRESSFILE" >> $slog

# Record to disk
$FFMPEGSOURCE/ffmpeg -hide_banner -loglevel error \
-progress $PROGRESSFILE -nostdin \
-re -i $SOURCE $APPENDPARAM \
$APARAM0 $VPARAM0 $RPARAM0 2>>$slog >>$RECFILE &

wait
