#!/usr/bin/env bats
load '../node_modules/bats-support/load'
load '../node_modules/bats-assert/load'

source "/opt/isha-streaming/isha-config.sh"

SN=unittest_offset_t1_stream1_session1
SN2=random_stream_name
SN3=unittest2_t1_stream1_session1
DATEFORMAT="%Y-%m-%dT%H:%M:%S%:z"

function setup() {
  # For safety, will first make sure RECDIR and LOGDIR exist
  if [[ -n $RECDIR ]] && [[ -n $LOGDIR ]]; then
	  # Recording
	  rm -f $RECDIR/${SN}.ts $RECDIR/${SN2}.ts $RECDIR/${SN3}.ts
	  # Record progress
	  rm -f $LOGDIR/${SN}_record_progress.log $LOGDIR/${SN2}_record_progress.log $LOGDIR/${SN3}_record_progress.log
	  # Record log
	  rm -f $LOGDIR/${SN}_record.log $LOGDIR/${SN2}_record.log $LOGDIR/${SN3}_record.log 
    # Transcode log
	  rm -f $LOGDIR/${SN}_transcode.log $LOGDIR/${SN2}_transcode.log $LOGDIR/${SN3}_transcode.log  
	fi
}

# @test "can access common function" {
# 	# Include common functions
# 	#script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# }

FFMPEGARGS="-y -hide_banner -loglevel error -nostdin -re -i $RECDIR/$TESTFILE -c copy -f flv"

# @test "should have correct offset after starting 1 minute late" {	
# 	echo "declare -A TIMINGS_unittest1_t1_session1=(\
# 	  [stream_start]=$($DATE -d '1 minutes ago'  +$DATEFORMAT)\
# 	 [session_start]=$($DATE -d '30 minutes'      +$DATEFORMAT)\
# 	   [session_end]=$($DATE -d '90 minutes'     +$DATEFORMAT)\
# 	    [stream_end]=$($DATE -d '120 minutes'     +$DATEFORMAT)\
# 	)" > /opt/isha-streaming/sessions/unittest.sh

# 	source "/opt/isha-streaming/isha-common.sh"

# 	DEST="rtmp://localhost:1935/testapp/$SN"
# 	$FFMPEGSOURCE/ffmpeg $FFMPEGARGS $DEST &

# 	run /opt/isha-streaming/isha-record.sh $SN testapp &
# 	pid=$!

# 	sleep 15
# 	read_progress_stats $(record_progress_filename $SN)
# 	kill $pid
 
#   echo "BITRATE = ${BITRATE}"
#   stripped=$(echo $BITRATE | sed "s/kbits\/s//")
# 	assert [ $(echo "${stripped} > 0" | bc) ]

# 	assert [ $(find ${RECDIR}/${SN}.ts -type f -size +1024c) ]

# 	local offset=$(calculate_offset $SN)

# 	# expected 2 seconds tolerance
# 	[ $offset -ge 60 ]
# 	[ $offset -le 70 ]
# 	echo "# Offset $offset within 10 seconds accuracy" >&3
	
# 	[ $offset -ge 60 ]
# 	[ $offset -le 62 ]
# 	echo "# Offset $offset within 2 seconds accuracy" >&3

# 	[ $offset -ge 60 ] 
# 	[ $offset -le 61 ]
# 	echo "# Offset $offset within 1 second accuracy" >&3
# }

# @test "should have correct offset if resuming once" {	
# 	echo "declare -A TIMINGS_unittest_offset_t1_stream1_session1=(\
# 	  [stream_start]=$($DATE -d '1 minutes ago'  +$DATEFORMAT)\
# 	 [session_start]=$($DATE -d '30 minutes'      +$DATEFORMAT)\
# 	   [session_end]=$($DATE -d '90 minutes'     +$DATEFORMAT)\
# 	    [stream_end]=$($DATE -d '120 minutes'     +$DATEFORMAT)\
# 	)" > /opt/isha-streaming/sessions/unittest.sh

# 	source "/opt/isha-streaming/isha-common.sh"

# 	DEST="rtmp://localhost:1935/testapp/$SN"
# 	$FFMPEGSOURCE/ffmpeg $FFMPEGARGS $DEST &
# 	echo "# first pid $!" >&3

# 	rm -f $LOGDIR/${SN}_record_progress.log
# 	/opt/isha-streaming/isha-record.sh $SN testapp &
# 	pid=$!

# 	sleep 10
# 	read_progress_stats $(record_progress_filename $SN)
# 	echo "# second pid $!" >&3
# 	echo "# jobs $(jobs -l)" >&3
	
# 	kill $pid
 
#   bitrate=$(echo $BITRATE | sed "s/kbits\/s//")
# 	assert [ $(echo "${bitrate} > 0.1" | bc) ]

# 	assert [ $(find ${RECDIR}/${SN}.ts -type f -size +65536c) ]

# 	# Clear stats
# 	rm -f $LOGDIR/${SN}_record_progress.log
# 	/opt/isha-streaming/isha-record.sh $SN testapp &
# 	pid=$!

# 	echo "# third pid $pid" >&3

# 	sleep 15
# 	read_progress_stats $(record_progress_filename $SN)
# 	kill $pid

# 	echo "BITRATE = $BITRATE"
#   bitrate=$(echo $BITRATE | sed "s/kbits\/s//")
# 	assert [ $(echo "${bitrate} > 0.1" | bc) ]

# 	local offset=$(calculate_offset $SN)

# 	# expected 2 seconds tolerance
# 	[ $offset -ge 75 ]
# 	[ $offset -le 85 ]
# 	echo "# Offset $offset within 10 seconds accuracy" >&3

# 	assert [ $offset -ge 75 ] && [ $offset -le 77 ]
# 	echo "# Offset $offset within 2 seconds accuracy" >&3
# 	assert [ $offset -ge 75 ] && [ $offset -le 76 ]
# 	echo "# Offset $offset within 1 second accuracy" >&3
# }

# @test "complicated" {	
# 	#should have correct offset if started before scheduled time, 
# 	#then stopped before sessions again started before session start, 
# 	#ended between session_start and session_end, 
# 	#started once again between that point and session_end

# 	echo "declare -A TIMINGS_unittest_offset_t1session1(\
# 	  [stream_start]=$($DATE -d '15 minutes'  +$DATEFORMAT)\
# 	 [session_start]=$($DATE -d '60 minutes'      +$DATEFORMAT)\
# 	   [session_end]=$($DATE -d '120 minutes'     +$DATEFORMAT)\
# 	    [stream_end]=$($DATE -d '165 minutes'     +$DATEFORMAT)\
# 	)" > /opt/isha-streaming/sessions/unittest.sh

# 	source "/opt/isha-streaming/isha-common.sh"
# 	DEST="rtmp://localhost:1935/testapp/$SN"
	
# 	# Play the source file
# 	$FFMPEGSOURCE/ffmpeg $FFMPEGARGS $DEST &
# 	# for i in {1..14}; do
# 	#  	# First broadcast
# 	#  	faketime -f "+${i}m" /opt/isha-streaming/isha-record.sh $SN testapp$i &
# 	#  	echo "# spawning process with time +${i}m" >&3
# 	#  	sleep 1
# 	# done

# 	faketime -f "+15m" "/opt/isha-streaming/isha-record.sh $SN testapp" &
# 	pid=$!
# 	echo "# details: $(ps -p $pid)"
# 	sleep 10
# 	echo "# details: $(ps -p $pid)"
# 	kill $pid



# 	# echo "# progress_id = $pid" >&3
#  #  sleep 1.8
#  #  sudo pkill -f "isha-record.sh"

# 	# read_progress_stats $(record_progress_filename $SN)
	
 
# 	# progress_time_ms=$OUT_TIME_MS
# 	# recorded_time_s=$(video_length_from_filename $RECDIR/${SN}.ts)

# 	# echo "# progress_time_ms = $OUT_TIME_MS recorded_time_s = $recorded_time_s" >&3

# 	# sleep 13

# 	# read_progress_stats $(record_progress_filename $SN)

# 	# kill $pid

# 	# progress_time_ms=$OUT_TIME_MS
# 	# recorded_time_s=$(video_length_from_filename $RECDIR/${SN}.ts)

# 	# echo "# progress_time_ms = $OUT_TIME_MS recorded_time_s = $recorded_time_s" >&3	



# 	# sleep 15
# 	# read_progress_stats $(record_progress_filename $SN)
# 	# kill $pid

# 	# echo "BITRATE = $BITRATE"
#  #  bitrate=$(echo $BITRATE | sed "s/kbits\/s//")
# 	# assert [ $(echo "${bitrate} > 0.1" | bc) ]

# 	# local offset=$(calculate_offset $SN)

# 	# # expected 2 seconds tolerance
# 	# [ $offset -ge 75 ]
# 	# [ $offset -le 85 ]
# 	# echo "# Offset $offset within 10 seconds accuracy" >&3


	
# 	# assert [ $offset -ge 75 ] && [ $offset -le 77 ]
# 	# echo "# Offset $offset within 2 seconds accuracy" >&3
# 	# assert [ $offset -ge 75 ] && [ $offset -le 76 ]
# 	# echo "# Offset $offset within 1 second accuracy" >&3


# }

# @test "should post stopped msg even when killed" {
# 	echo "todo"
# }

teardown() {
	rm -f /opt/isha-streaming/sessions/unittest.sh
	# Kill all spawned processes
	pkill -P $$ || true
}