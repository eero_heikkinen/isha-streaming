#!/usr/bin/env bats
set -e
load '../node_modules/bats-support/load'
load '../node_modules/bats-assert/load'

source "/opt/isha-streaming/isha-config.sh"

SN=placeholdertest_t1_stream1_session1
SN2=random_stream_name
SN3=unittest2_t1_stream1_session1
DATEFORMAT="%Y-%m-%dT%H:%M:%S%:z"

function setup() {
  # For safety, will first make sure RECDIR and LOGDIR exist
  if [[ -n $RECDIR ]] && [[ -n $LOGDIR ]]; then
	  # recordings
	  rm -f $RECDIR/placeholdertest*
	  # logs
	  rm -f $LOGDIR/placeholdertest*	  
	fi

	source "/opt/isha-streaming/isha-common.sh"
	validate_config
}

@test "Stream that has started but not broadcasting should have placeholder video starting" {	
	SESSION_ID=placeholdertest_t1_session1
	SN=placeholdertest_t1_stream1_session1

	echo "declare -A TIMINGS_${SESSION_ID}=(\
	  [stream_start]=$($DATE -d '1 minutes ago'  +$DATEFORMAT)\
	 [session_start]=$($DATE -d '30 minutes'      +$DATEFORMAT)\
	   [session_end]=$($DATE -d '90 minutes'     +$DATEFORMAT)\
	    [stream_end]=$($DATE -d '120 minutes'     +$DATEFORMAT)\
	    [streams]=stream1
	)" > /opt/isha-streaming/sessions/unittest.sh

	source "/opt/isha-streaming/isha-common.sh"

	# Ensure stream is not playing
	PIDFILE=$(placeholder_pid_filename $SN)
  [[ ! -e $PIDFILE ]]

  # Ensure nothing on HLS
  HLS_UPDATE_TIME=$(test_hls)
	[[ $HLS_UPDATE_TIME == 'N/A' ]] || [[ $HLS_UPDATE_TIME -ge 5 ]] 

	run /opt/isha-streaming/isha-heartbeat.sh

	printf '# %s\n' "${lines[@]}"
	printf '%s\n' "${lines[@]}" | grep "$SESSION_ID in status before_session_start"
	printf '%s\n' "${lines[@]}" | grep "going through stream $SN"
	printf '%s\n' "${lines[@]}" | grep "placeholder for $SN"

	echo "Testing with $SN : placeholder video should appear on DASH & HLS">>$LOGDIR/isha-monitoring.log
	
	# May have to adjust this: this is enough time on 2015 macbook pro i7 to propagate everything
	sleep 20

	PIDFILE=$(placeholder_pid_filename $SN)
	# Ensure placeholder stream has started
  echo "making sure $PIDFILE exists"
  [[ -e $PIDFILE ]]
  echo "making sure referenced process running"
  ps -p $(cat $PIDFILE)

  # Ensure transcoding
  read_progress_stats $(transcode_progress_filename $SN) 
  [[ $PROGRESS_UPDATED -ge 0 ]]
  [[ $PROGRESS_UPDATED -lt 5 ]]

  echo "BITRATE = ${BITRATE}"
  stripped=$(echo $BITRATE | sed "s/kbits\/s//")
  assert [ $(echo "${stripped} > 0" | bc) ]

  # Ensure recording
  read_progress_stats $(record_progress_filename $SN) 
  [[ $PROGRESS_UPDATED -ge 0 ]]
  [[ $PROGRESS_UPDATED -lt 5 ]]

  echo "BITRATE = ${BITRATE}"
  stripped=$(echo $BITRATE | sed "s/kbits\/s//")
  assert [ $(echo "${stripped} > 0" | bc) ]

  # Ensure we are live on HLS
  HLS_UPDATE_TIME=$(test_hls $SN)
  echo "HLS UPDATE TIME: $HLS_UPDATE_TIME"
  [[ $HLS_UPDATE_TIME != 'N/A' ]]
  [[ $HLS_UPDATE_TIME -ge 0 ]]
  [[ $HLS_UPDATE_TIME -lt 15 ]]

	#printf '%s\n' "${lines[@]}">>/tmp/log/isha-monitoring.log
}

@test "Iterates over the correct language streams" {	
	echo "declare -A TIMINGS_placeholdertest_t1_session1=(\
	  [stream_start]=$($DATE -d '1 minutes ago'  +$DATEFORMAT)\
	 [session_start]=$($DATE -d '30 minutes'      +$DATEFORMAT)\
	   [session_end]=$($DATE -d '90 minutes'     +$DATEFORMAT)\
	    [stream_end]=$($DATE -d '120 minutes'     +$DATEFORMAT)\
	    [streams]=stream1,stream2,stream5
	)" > /opt/isha-streaming/sessions/unittest.sh

	source "/opt/isha-streaming/isha-common.sh"

	# This will fail if string not found
	run /opt/isha-streaming/isha-heartbeat.sh

	printf '%s\n' "${lines[@]}" | grep "going through stream placeholdertest_t1_stream1_session1"
	printf '%s\n' "${lines[@]}" | grep "going through stream placeholdertest_t1_stream2_session1"
	printf '%s\n' "${lines[@]}" | grep "going through stream placeholdertest_t1_stream5_session1"
	
	# this should not be there
	! printf '%s\n' "${lines[@]}" | grep "going through stream placeholdertest_t1_stream3_session1"
}

@test "When connecting, already playing placeholder video should stop" {	
	SESSION_ID=placeholdertest2_t1_session1
	SN=placeholdertest2_t1_stream5_session1

	echo "declare -A TIMINGS_${SESSION_ID}=(\
	  [stream_start]=$($DATE -d '1 minutes ago'  +$DATEFORMAT)\
	 [session_start]=$($DATE -d '30 minutes'      +$DATEFORMAT)\
	   [session_end]=$($DATE -d '90 minutes'     +$DATEFORMAT)\
	    [stream_end]=$($DATE -d '120 minutes'     +$DATEFORMAT)\
	    [streams]=stream5
	)" > /opt/isha-streaming/sessions/unittest.sh

	source "/opt/isha-streaming/isha-common.sh"

	# Ensure stream is not playing
	PIDFILE=$(placeholder_pid_filename $SN)
  [[ ! -e $PIDFILE ]]

  echo "# Running heartbeat - should pick up a missing stream and play the placeholder" >&3
	run /opt/isha-streaming/isha-heartbeat.sh

	printf '%s\n' "${lines[@]}" | grep "$SESSION_ID in status before_session_start"
	printf '%s\n' "${lines[@]}" | grep "Playing placeholder for $SN"

	sleep 60

	# Ensure placeholder stream has started
  echo "making sure $PIDFILE exist"
  [[ -e $PIDFILE ]]
  placeholder_pid=$(cat $PIDFILE) 
  echo "making sure referenced process running"
  ps -p $placeholder_pid

  echo "# Placeholder has started" >&3

  # Let's now stream to the actual application
  
  DEST="rtmp://localhost:1935/source/$SN"

  cmd="$FFMPEGSOURCE/ffmpeg -y -hide_banner -loglevel info -nostdin -re \
  -i $RECDIR/$TESTFILE -c copy -f flv rtmp://localhost:1935/source/$SN"
	echo $cmd
	echo "# Now simulating stream - placeholder should get replaced" >&3
	$cmd &
	pid=$!
	
	sleep 3
	echo "making sure new player didn't exit"
	ps -p $pid

	sleep 15

	# The placeholder process should have ended
	! ps -p $placeholder_pid

	sleep 45

	# Ensure transcoding
  read_progress_stats $(transcode_progress_filename $SN) 
  echo "PROGRESS UPDATED $PROGRESS_UPDATED"
  [[ $PROGRESS_UPDATED -ge 0 ]]
  [[ $PROGRESS_UPDATED -lt 5 ]]

  # Ensure recording
  read_progress_stats $(record_progress_filename $SN) 
  echo "PROGRESS UPDATED $PROGRESS_UPDATED"
  [[ $PROGRESS_UPDATED -ge 0 ]]
  [[ $PROGRESS_UPDATED -lt 5 ]]

  # Ensure we are live on HLS
  HLS_UPDATE_TIME=$(test_hls $SN)
  echo "HLS UPDATE TIME: $HLS_UPDATE_TIME"
  [[ $HLS_UPDATE_TIME -ge 0 ]]
  [[ $HLS_UPDATE_TIME -lt 5 ]]  

  echo "# Live on HLS" >&3

  kill $pid

  # TODO: THIS DID NOT DETECT WHEN NEW PLAYER ACTUALLY DID NOT START 
  # 
  # ensure recorded time seems about right (at least 110 seconds)
  
  RECFILE=$RECDIR/${SN}.ts
  RECORDED_SECONDS=$(video_length_from_filename $RECFILE)

  [[ ${RECORDED_SECONDS%.*} -ge 5 ]]

  echo "# Recorded over 100 seconds" >&3

  echo "# Now playing recorded files as a delayed stream" >&3

  TSN=placeholdertest2_t2_stream1_session1

	cmd="/opt/isha-streaming/isha-vbr.sh -s $SN -t $TSN"
	echo "running $cmd"
	run $cmd &

	pid=$!
	sleep 35

	# Check HLS status
	local hls_status=$(test_hls $TSN)

  [[ $hls_status != 'N/A' ]]

	kill $pid
}


@test "Placeholder should work even for multiple disconnects" {	
	SESSION_ID=placeholdertest3_t1_session1
	SN=placeholdertest3_t1_stream5_session1

	echo "declare -A TIMINGS_${SESSION_ID}=(\
	  [stream_start]=$($DATE -d '1 minutes ago'  +$DATEFORMAT)\
	 [session_start]=$($DATE -d '30 minutes'      +$DATEFORMAT)\
	   [session_end]=$($DATE -d '90 minutes'     +$DATEFORMAT)\
	    [stream_end]=$($DATE -d '120 minutes'     +$DATEFORMAT)\
	    [streams]=stream5
	)" > /opt/isha-streaming/sessions/unittest.sh

	source "/opt/isha-streaming/isha-common.sh"

	# Ensure player is not playing
	PIDFILE=$(placeholder_pid_filename $SN)
  [[ ! -e $PIDFILE ]]

  echo "# Running heartbeat - should pick up a missing stream and play the placeholder" >&3
	run /opt/isha-streaming/isha-heartbeat.sh

	printf '%s\n' "${lines[@]}" | grep "$SESSION_ID in status before_session_start"
	printf '%s\n' "${lines[@]}" | grep "Playing placeholder for $SN"

	sleep 60

	# Ensure placeholder stream has started
  echo "making sure $PIDFILE exist"
  [[ -e $PIDFILE ]]
  placeholder_pid=$(cat $PIDFILE) 
  echo "making sure referenced process running"
  ps -p $placeholder_pid

  echo "# Placeholder has started" >&3

  # Let's now stream to the actual application
  
  DEST="rtmp://localhost:1935/source/$SN"

  cmd="$FFMPEGSOURCE/ffmpeg -y -hide_banner -loglevel info -nostdin -re \
  -i $RECDIR/$TESTFILE -c copy -f flv rtmp://localhost:1935/source/$SN"
	echo $cmd
	echo "# Now simulating stream - placeholder should get replaced" >&3
	$cmd &
	pid=$!
	
	sleep 3
	echo "making sure new player didn't exit"
	ps -p $pid

	sleep 15

	# The placeholder process should have ended
	! ps -p $placeholder_pid

	sleep 45

	kill $pid

	sleep 30

	run /opt/isha-streaming/isha-heartbeat.sh

	printf '%s\n' "${lines[@]}" | grep "$SESSION_ID in status before_session_start"
	printf '%s\n' "${lines[@]}" | grep "Playing placeholder for $SN"

	sleep 60

	# Ensure placeholder stream has started
  echo "making sure $PIDFILE exist"
  [[ -e $PIDFILE ]]
  placeholder_pid=$(cat $PIDFILE) 
  echo "making sure referenced process running"
  ps -p $placeholder_pid

  echo "# Placeholder has started" >&3

  DEST="rtmp://localhost:1935/source/$SN"

  cmd="$FFMPEGSOURCE/ffmpeg -y -hide_banner -loglevel info -nostdin -re \
  -i $RECDIR/$TESTFILE -c copy -f flv rtmp://localhost:1935/source/$SN"
	echo $cmd
	echo "# Now simulating stream - placeholder should get replaced" >&3
	$cmd &
	pid=$!
	
	sleep 3
	echo "making sure new player didn't exit"
	ps -p $pid

	sleep 60

	# The placeholder process should have ended
	! ps -p $placeholder_pid

	# Ensure transcoding
  read_progress_stats $(transcode_progress_filename $SN) 
  echo "PROGRESS UPDATED $PROGRESS_UPDATED"
  [[ $PROGRESS_UPDATED -ge 0 ]]
  [[ $PROGRESS_UPDATED -lt 5 ]]

  # Ensure recording
  read_progress_stats $(record_progress_filename $SN) 
  echo "PROGRESS UPDATED $PROGRESS_UPDATED"
  [[ $PROGRESS_UPDATED -ge 0 ]]
  [[ $PROGRESS_UPDATED -lt 5 ]]

  # Ensure we are live on HLS
  HLS_UPDATE_TIME=$(test_hls $SN)
  echo "HLS UPDATE TIME: $HLS_UPDATE_TIME"
	[[ $HLS_UPDATE_TIME -ge 0 ]]
  [[ $HLS_UPDATE_TIME -lt 5 ]]  

  echo "# Live on HLS" >&3

  kill $pid

  # ensure recorded time seems about right (at least 110 seconds)
  
  RECFILE=$RECDIR/${SN}.ts
  RECORDED_SECONDS=$(video_length_from_filename $RECFILE)

  [[ ${RECORDED_SECONDS%.*} -ge 5 ]]

  echo "# Recorded over 110 seconds" >&3

  echo "# Now playing recorded files as a delayed stream" >&3

  TSN=placeholdertest3_t2_stream1_session1

	cmd="/opt/isha-streaming/isha-vbr.sh -s $SN -t $TSN"
	echo "running $cmd"
	run $cmd &

	pid=$!
	sleep 30

	# Check HLS status
	local hls_status=$(test_hls $TSN)

 	assert [ ${hls_status} != 'N/A' ]
	assert [ $(echo "${hls_status} < 10" | bc) ]

	kill $pid
}

# @test "When delayed stream gets stopped, it should be restarted with correct seeking" {	
# 	SESSION_ID=placeholdertest2_t1_session1
# 	SESSION_ID_DELAYED=placeholdertest2_t2_session1

# 	SN=placeholdertest2_t1_stream1_session1
# 	SN_DELAYED=unittest_player1_t1_stream1_session1

# 	# Ensure stream is not playing
# 	PIDFILE=$(placeholder_pid_filename $SN)
#   [[ ! -e $PIDFILE ]]

#   echo "declare -A TIMINGS_${SESSION_ID}=(\
# 	  [stream_start]=$($DATE -d '0 minutes' +$DATEFORMAT)\
# 	 [session_start]=$($DATE -d '5 minutes'     +$DATEFORMAT)\
# 	   [session_end]=$($DATE -d '10 minutes'     +$DATEFORMAT)\
# 	    [stream_end]=$($DATE -d '15 minutes'     +$DATEFORMAT)\
# 	    [streams]=stream1
# 	);\
# 	declare -A TIMINGS_unittest_${SESSION_ID_DELAYED}=(\
# 	  [stream_start]=$($DATE -d '5 minutes'      +$DATEFORMAT)\
# 	 [session_start]=$($DATE -d '10 minutes'     +$DATEFORMAT)\
# 	   [session_end]=$($DATE -d '15 minutes'     +$DATEFORMAT)\
# 	    [stream_end]=$($DATE -d '20 minutes'     +$DATEFORMAT)\
# 	    [streams]=stream1
# 	)" > /opt/isha-streaming/sessions/unittest.sh
	
# 	source "/opt/isha-streaming/isha-common.sh"

	
# 	RECEIVE="rtmp://localhost:1935/receive/"
	
# 	cmd="/opt/isha-streaming/isha-rtmp.sh $TESTFILE $RECEIVE $SN1"
# 	echo "running $cmd"
# 	run $cmd &
# 	echo "status = ${status}"
#   echo "output = ${output}"
#   #
# 	pid=$!
# 	sleep 15
# 	kill $pid

# 	assert [ $(find ${RECDIR}/${SN1}.ts -type f -size +524288c) ]
# 	assert [ $(find ${RECDIR}/${SN1}_max.ts -type f -size +262144c) ]
# 	assert [ $(find ${RECDIR}/${SN1}_best.ts -type f -size +262144c) ]
# 	assert [ $(find ${RECDIR}/${SN1}_high.ts -type f -size +131072c) ]
# 	assert [ $(find ${RECDIR}/${SN1}_med.ts -type f -size +65536c) ]
# 	assert [ $(find ${RECDIR}/${SN1}_low.ts -type f -size +32768c) ]
# 	assert [ $(find ${RECDIR}/${SN1}_bad.ts -type f -size +32768c) ]
# 	assert [ $(find ${RECDIR}/${SN1}_worst.ts -type f -size +32768c) ]

# 	echo "Files have been successfully created"


# 	# play vbr delayed stream

# 	DSN1=unittest_player1_t2_stream1_session1
# 	DASH="rtmp://localhost:1935/dash/"

# 	local cmd="/opt/isha-streaming/isha-vbr.sh -s $SN1 -t $DSN1 -l"
# 	echo "running $cmd"
# 	run $cmd &

# 	pid=$!
# 	sleep 15

# 	# Check HLS status
# 	local hls_status=$(test_hls $DSN1)

#  	assert [ ${hls_status:-} != 'N/A' ]

#  	# simulate restart of server: stop vbr stream 
#  	# 
#  	kill $pid

#  	sleep 5

#  	# Now let's simulate heartbeat.sh to bring it back up
 	
#  	# TODO: correct time for delayed stream start
#  	#FAKETIME_FMT=%s faketime -f "$unix_timestamp $multiplier" /opt/isha-streaming/isha-record.sh $individual_sn testapp &
# 	run /opt/isha-streaming/isha-heartbeat.sh

# 	printf '%s\n' "${lines[@]}" | grep "$SESSION_ID_DELAYED in status streaming"
# 	printf '%s\n' "${lines[@]}" | grep "going through stream $SN"
# 	printf '%s\n' "${lines[@]}" | grep "Live stream offline: $SN"
# 	printf '%s\n' "${lines[@]}" | grep "Playing placeholder file"


# 	sleep 10

# 	# VERIFY VBR IS PLAYING WITH CORRECT OFFSET

# 	# Ensure delayed stream has started
#   echo "making sure $PIDFILE exist"
#   [[ -e $PIDFILE ]]
#   echo "making sure referenced process running"
#   ps -p $(cat $PIDFILE)


#   # Let's now stream to the actual application
  
#   DEST="rtmp://localhost:1935/source/$SN"
#   FFMPEGARGS="-y -hide_banner -loglevel info -nostdin -re -i $RECDIR/$TESTFILE -c copy -f flv"
# 	$FFMPEGSOURCE/ffmpeg $FFMPEGARGS $DEST &

# 	sleep 5  

# 	# The placeholder process should have ended
# 	! ps -p $(cat $PIDFILE)

# 	sleep 15

# 	# Ensure transcoding
#   read_progress_stats $(transcode_progress_filename $SN) 
#   echo "PROGRESS UPDATED $PROGRESS_UPDATED"
#   [[ $PROGRESS_UPDATED -ge 0 ]]
#   [[ $PROGRESS_UPDATED -lt 5 ]]

#   # Ensure recording
#   read_progress_stats $(record_progress_filename $SN) 
#   echo "PROGRESS UPDATED $PROGRESS_UPDATED"
#   [[ $PROGRESS_UPDATED -ge 0 ]]
#   [[ $PROGRESS_UPDATED -lt 5 ]]

#   # Ensure we are live on HLS
#   HLS_UPDATE_TIME=$(test_hls $SN)
#   echo "HLS UPDATE TIME: $HLS_UPDATE_TIME"
# 	[[ $HLS_UPDATE_TIME -ge 0 ]]
#   [[ $HLS_UPDATE_TIME -lt 5 ]]  

#   # note: some reason I see cat: /tmp/placeholdertest_t1_stream1_session1.pid: No such file or directory 
#   # in the logs
# }

# @test "Delayed session should have delayedplayer starting, unless already playing" {	
# 	source "/opt/isha-streaming/isha-common.sh"

# 	exec "/opt/isha-streaming/isha-heartbeat.sh"
# }

teardown() {
	# Delete mock configuration settings
	rm -f /opt/isha-streaming/sessions/unittest.sh
	# Kill all spawned processes
	pkill -P $$ || true
	pkill -f placeholdertest || true
}