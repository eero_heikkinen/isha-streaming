#!/usr/bin/env bats
load '../node_modules/bats-support/load'
load '../node_modules/bats-assert/load'

source "/opt/isha-streaming/isha-config.sh"

FFMPEGARGS="-y -hide_banner -loglevel error -nostdin -stream_loop -1 -re -i $TESTFILE -c copy -f flv"
SN1=unittest_player1_t1_stream1_session1
DSN1=unittest_player1_t2_stream1_session1
SN2=unittest_player2_t1_stream1_session1
SN3=unittest_player3_t1_stream1_session1
DATEFORMAT="%Y-%m-%dT%H:%M:%S%:z"

function setup() {
  # For safety, will first make sure RECDIR and LOGDIR exist
  if [[ -n $RECDIR ]] && [[ -n $LOGDIR ]]; then
	  # Recording
	  rm -f $RECDIR/${SN1}.ts $RECDIR/${SN2}.ts $RECDIR/${SN3}.ts $RECDIR/${DSN1}.ts
	  rm -f $RECDIR/${SN1}_*.ts $RECDIR/${SN2}_*.ts $RECDIR/${SN3}-*.ts
	  # Record progress
	  rm -f $LOGDIR/${SN1}_record_progress.log $LOGDIR/${SN2}_record_progress.log $LOGDIR/${SN3}_record_progress.log $RECDIR/${DSN1}_record_progress.log
	  # Record log
	  rm -f $LOGDIR/${SN1}_record.log $LOGDIR/${SN2}_record.log $LOGDIR/${SN3}_record.log $LOGDIR/${DSN1}_record.log 
    # Transcode log
	  rm -f $LOGDIR/${SN1}_transcode.log $LOGDIR/${SN2}_transcode.log $LOGDIR/${SN3}_transcode.log  $LOGDIR/${DSN1}_transcode.log  

	  rm -f /opt/isha-streaming/sessions/unittest.sh
	fi

	assert [ $(find $RECDIR/$TESTFILE -type f -size +1024c) ]
}

@test "Playing a delayed stream from the master file" {	
	echo "declare -A TIMINGS_unittest_player1_t2_session1=(\
	  [stream_start]=$($DATE -d '0 minutes' +$DATEFORMAT)\
	 [session_start]=$($DATE -d '45 minutes'     +$DATEFORMAT)\
	   [session_end]=$($DATE -d '105 minutes'     +$DATEFORMAT)\
	    [stream_end]=$($DATE -d '150 minutes'     +$DATEFORMAT)\
	)" > /opt/isha-streaming/sessions/unittest.sh
	source "/opt/isha-streaming/isha-common.sh"

	run /opt/isha-streaming/isha-rtmp.sh -s $TESTFILE -d rtmp://localhost:1935/receive/ -t $DSN1 -l &
	pid=$!

	#echo "status = ${status}"
  #echo "output = ${output}"
 
	sleep 10
	kill $pid

	read_progress_stats $LOGDIR/${DSN1}_rtmp_progress.log
  stripped=$(echo $BITRATE | sed "s/kbits\/s//")

  # TODO: Check it's not recording
  # TODO: check with ffprobe do we have both audio and video
  
	assert [ $(echo "${stripped} > 0" | bc) ]
}

@test "YouTube simulation: playing from master file to /testapp" {	
	echo "declare -A TIMINGS_unittest_player1_t2_session1=(\
	  [stream_start]=$($DATE -d '0 minutes' +$DATEFORMAT)\
	 [session_start]=$($DATE -d '45 minutes'     +$DATEFORMAT)\
	   [session_end]=$($DATE -d '105 minutes'     +$DATEFORMAT)\
	    [stream_end]=$($DATE -d '150 minutes'     +$DATEFORMAT)\
	)" > /opt/isha-streaming/sessions/unittest.sh
	source "/opt/isha-streaming/isha-common.sh"

	run /opt/isha-streaming/isha-rtmp.sh -s $TESTFILE -d "rtmp://localhost:1935/testapp/" -t $SN1 &
	pid=$!
	sleep 10
	kill $pid

	read_progress_stats $LOGDIR/${SN1}_rtmp_progress.log
 
  echo "BITRATE = ${BITRATE}"
  stripped=$(echo $BITRATE | sed "s/kbits\/s//")
	assert [ $(echo "${stripped} > 0" | bc) ]
}

@test "broadcast to /receive, transcode to vbr and play vbr files as delayed stream" {	
	echo "declare -A TIMINGS_unittest_player1_t1_session1=(\
	  [stream_start]=$($DATE -d '0 minutes' +$DATEFORMAT)\
	 [session_start]=$($DATE -d '5 minutes'     +$DATEFORMAT)\
	   [session_end]=$($DATE -d '10 minutes'     +$DATEFORMAT)\
	    [stream_end]=$($DATE -d '15 minutes'     +$DATEFORMAT)\
	);\
	declare -A TIMINGS_unittest_player1_t2_session1=(\
	  [stream_start]=$($DATE -d '5 minutes' +$DATEFORMAT)\
	 [session_start]=$($DATE -d '10 minutes'     +$DATEFORMAT)\
	   [session_end]=$($DATE -d '15 minutes'     +$DATEFORMAT)\
	    [stream_end]=$($DATE -d '20 minutes'     +$DATEFORMAT)\
	)" > /opt/isha-streaming/sessions/unittest.sh
	
	source "/opt/isha-streaming/isha-common.sh"

	SN1=unittest_player1_t1_stream1_session1
	
	cmd="/opt/isha-streaming/isha-rtmp.sh -s $TESTFILE -d rtmp://localhost:1935/receive/ -t $SN1"
	echo "running $cmd"
	run $cmd &
	echo "status = ${status}"
  echo "output = ${output}"
  #
	pid=$!
	sleep 30
	kill $pid

	assert [ $(find ${RECDIR}/${SN1}.ts -type f -size +524288c) ]
	assert [ $(find ${RECDIR}/${SN1}_max.ts -type f -size +262144c) ]
	assert [ $(find ${RECDIR}/${SN1}_best.ts -type f -size +262144c) ]
	assert [ $(find ${RECDIR}/${SN1}_high.ts -type f -size +131072c) ]
	assert [ $(find ${RECDIR}/${SN1}_med.ts -type f -size +65536c) ]
	assert [ $(find ${RECDIR}/${SN1}_low.ts -type f -size +32768c) ]
	assert [ $(find ${RECDIR}/${SN1}_bad.ts -type f -size +32768c) ]
#	assert [ $(find ${RECDIR}/${SN1}_worst.ts -type f -size +32768c) ]

	echo "Files have been successfully created"

	# We have now the proper 7 transcoded files in RECDIR
	# Let's try streaming them to /dash
	
	DSN1=unittest_player1_t2_stream1_session1
	DASH="rtmp://localhost:1935/dash/"

	local cmd="/opt/isha-streaming/isha-vbr.sh -s $SN1 -t $DSN1 -l"
	echo "running $cmd"
	run $cmd &

	pid=$!
	sleep 20

	# Check HLS status
	local hls_status=$(test_hls $DSN1)
	kill $pid

 	assert [ ${hls_status:-} != 'N/A' ]
	assert [ $(echo "${hls_status} < 10" | bc) ]
}


@test "play vbr files as delayed stream, with from param" {	
	echo "declare -A TIMINGS_unittest_player1_t1_session1=(\
	  [stream_start]=$($DATE -d '0 minutes' +$DATEFORMAT)\
	 [session_start]=$($DATE -d '5 minutes'     +$DATEFORMAT)\
	   [session_end]=$($DATE -d '10 minutes'     +$DATEFORMAT)\
	    [stream_end]=$($DATE -d '15 minutes'     +$DATEFORMAT)\
	);\
	declare -A TIMINGS_unittest_player1_t2_session1=(\
	  [stream_start]=$($DATE -d '5 minutes' +$DATEFORMAT)\
	 [session_start]=$($DATE -d '10 minutes'     +$DATEFORMAT)\
	   [session_end]=$($DATE -d '15 minutes'     +$DATEFORMAT)\
	    [stream_end]=$($DATE -d '20 minutes'     +$DATEFORMAT)\
	)" > /opt/isha-streaming/sessions/unittest.sh
	
	source "/opt/isha-streaming/isha-common.sh"

	SN1=unittest_player1_t1_stream1_session1
	
	cmd="/opt/isha-streaming/isha-rtmp.sh -s $TESTFILE -d rtmp://localhost:1935/receive/ -t $SN1"
	echo "running $cmd"
	run $cmd &

	pid=$!
	sleep 30
	kill $pid

	assert [ $(find ${RECDIR}/${SN1}.ts -type f -size +524288c) ]
	assert [ $(find ${RECDIR}/${SN1}_max.ts -type f -size +262144c) ]
	assert [ $(find ${RECDIR}/${SN1}_best.ts -type f -size +262144c) ]
	assert [ $(find ${RECDIR}/${SN1}_high.ts -type f -size +131072c) ]
	assert [ $(find ${RECDIR}/${SN1}_med.ts -type f -size +65536c) ]
	assert [ $(find ${RECDIR}/${SN1}_low.ts -type f -size +32768c) ]
	assert [ $(find ${RECDIR}/${SN1}_bad.ts -type f -size +32768c) ]
#	assert [ $(find ${RECDIR}/${SN1}_worst.ts -type f -size +32768c) ]

	echo "Files have been successfully created"

	# We have now the proper 7 transcoded files in RECDIR
	# Let's try streaming them to /dash
	
	DSN1=unittest_player1_t2_stream1_session1
	DASH="rtmp://localhost:1935/dash/"

	local cmd="/opt/isha-streaming/isha-vbr.sh -s $SN1 -t $DSN1 -l -f 15"
	echo "running $cmd"
	run $cmd &

	pid=$!
	sleep 15

	# Check HLS status
	local hls_status=$(test_hls $DSN1)
	kill $pid

 	assert [ ${hls_status:-} != 'N/A' ]
	assert [ $(echo "${hls_status} < 10" | bc) ]
}

@test "streaming to stream_end stream name should work" {	
	echo "declare -A TIMINGS_unittest_player1_t1_session1=(\
	  [stream_start]=$($DATE -d '0 minutes' +$DATEFORMAT)\
	 [session_start]=$($DATE -d '5 minutes'     +$DATEFORMAT)\
	   [session_end]=$($DATE -d '10 minutes'     +$DATEFORMAT)\
	    [stream_end]=$($DATE -d '15 minutes'     +$DATEFORMAT)\
	);\
	declare -A TIMINGS_unittest_player1_t2_session1=(\
	  [stream_start]=$($DATE -d '5 minutes' +$DATEFORMAT)\
	 [session_start]=$($DATE -d '10 minutes'     +$DATEFORMAT)\
	   [session_end]=$($DATE -d '15 minutes'     +$DATEFORMAT)\
	    [stream_end]=$($DATE -d '20 minutes'     +$DATEFORMAT)\
	)" > /opt/isha-streaming/sessions/unittest.sh
	
	source "/opt/isha-streaming/isha-common.sh"

	SN1=unittest_player1_t1_stream1_session1

	/opt/isha-streaming/isha-rtmp.sh -s $TESTFILE -d "rtmp://localhost:1935/receive/" -t $SN1 &
	

	sleep 30
	pid=$(cat $(player_pid_filename $SN1))
	echo "trying to kill process with pid $pid">>$LOGDIR/isha-monitoring.log
	pkill -F $(player_pid_filename $SN1)
	! ps -p $pid

	sleep 10

	assert [ -e ${RECDIR}/${SN1}.ts ]	
	assert [ -e ${RECDIR}/${SN1}_max.ts ]	
	assert [ -e ${RECDIR}/${SN1}_best.ts ]	
	assert [ -e ${RECDIR}/${SN1}_high.ts ]
	assert [ -e ${RECDIR}/${SN1}_med.ts ]
	assert [ -e ${RECDIR}/${SN1}_low.ts ]
	assert [ -e ${RECDIR}/${SN1}_bad.ts ]
#	assert [ -e ${RECDIR}/${SN1}_worst.ts ]	

	# We have now the proper 7 transcoded files in RECDIR
	# Let's try streaming them to /dash
	
	/opt/isha-streaming/isha-vbr.sh -s $SN1 -t $SN1 -l &
	pid2=$!

	sleep 10
	kill $pid2

	# Check HLS status
	local hls_status=$(test_hls $SN1)
 	assert [ ${hls_status:-} != 'N/A' ]
	assert [ $(echo "${hls_status} < 10" | bc) ]
}

teardown() {
	rm -f /opt/isha-streaming/sessions/unittest.sh
	# Kill all spawned processes
	pkill -P $$ || true
  pkill -f unittest || true

  rm -f $RECDIR/unittest_player*.ts
}
