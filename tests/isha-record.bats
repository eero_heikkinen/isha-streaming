#!/usr/bin/env bats
load '../node_modules/bats-support/load'
load '../node_modules/bats-assert/load'

source "/opt/isha-streaming/isha-config.sh"

SN=unittest1_t1_stream1_session1
SN2=random_stream_name
SN3=unittest2_t1_stream1_session1
DATEFORMAT="%Y-%m-%dT%H:%M:%S%:z"

function setup() {
  # For safety, will first make sure RECDIR and LOGDIR exist
  if [[ -n $RECDIR ]] && [[ -n $LOGDIR ]]; then
	  # Recording
	  rm -f $RECDIR/${SN}.ts $RECDIR/${SN2}.ts $RECDIR/${SN3}.ts
	  # Record progress
	  rm -f $LOGDIR/${SN}_record_progress.log $LOGDIR/${SN2}_record_progress.log $LOGDIR/${SN3}_record_progress.log
	  # Record log
	  rm -f $LOGDIR/${SN}_record.log $LOGDIR/${SN2}_record.log $LOGDIR/${SN3}_record.log 
    # Transcode log
	  rm -f $LOGDIR/${SN}_transcode.log $LOGDIR/${SN2}_transcode.log $LOGDIR/${SN3}_transcode.log  
	fi
}

# @test "can access common function" {
# 	# Include common functions
# 	#script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# }

FFMPEGARGS="-y -hide_banner -loglevel info -nostdin -re -i $RECDIR/$TESTFILE -c copy -f flv"

@test "should record from a scheduled stream name" {	
	echo "declare -A TIMINGS_unittest1_t1_session1=(\
	  [stream_start]=$($DATE -d '10 minutes ago'  +$DATEFORMAT)\
	 [session_start]=$($DATE -d '30 minutes'      +$DATEFORMAT)\
	   [session_end]=$($DATE -d '90 minutes'     +$DATEFORMAT)\
	    [stream_end]=$($DATE -d '120 minutes'     +$DATEFORMAT)\
	)" > /opt/isha-streaming/sessions/unittest.sh

	source "/opt/isha-streaming/isha-common.sh"

	DEST="rtmp://localhost:1935/testapp/$SN"
	$FFMPEGSOURCE/ffmpeg $FFMPEGARGS $DEST &

	run /opt/isha-streaming/isha-record.sh $SN testapp &

	sleep 10

	read_progress_stats $(record_progress_filename $SN)
 
  echo "BITRATE = ${BITRATE}"
  stripped=$(echo $BITRATE | sed "s/kbits\/s//")
	assert [ $(echo "${stripped} > 0" | bc) ]

	# Check file exists and larger than 1024 bytes
	assert [ $(find ${RECDIR}/${SN}.ts -type f -size +1024c) ]
}

@test "should not record from stream not yet started, but after starting should" {
	echo "declare -A TIMINGS_unittest2_t1_session1=(\
	  [stream_start]=$($DATE -d '2 minutes' +$DATEFORMAT)\
	 [session_start]=$($DATE -d '60 minutes'     +$DATEFORMAT)\
	   [session_end]=$($DATE -d '120 minutes'     +$DATEFORMAT)\
	    [stream_end]=$($DATE -d '180 minutes'     +$DATEFORMAT)\
	)" > /opt/isha-streaming/sessions/unittest.sh

	source "/opt/isha-streaming/isha-common.sh"

	DEST="rtmp://localhost:1935/testapp/$SN3"
	$FFMPEGSOURCE/ffmpeg $FFMPEGARGS $DEST &
	#sleep 3

	run /opt/isha-streaming/isha-record.sh $SN3 testapp &

	sleep 10

	read_progress_stats $(record_progress_filename $SN3)
 
  
  stripped=$(echo $BITRATE | sed "s/kbits\/s//")
  # We should not have anything recorded
	assert [ ! $(echo "${stripped} > 0" | bc) ]

	# Move time forward by 3 minutes
	run faketime -f '+3m' /opt/isha-streaming/isha-record.sh $SN3 testapp &

	sleep 10

	read_progress_stats $(record_progress_filename $SN3)
 
  stripped=$(echo $BITRATE | sed "s/kbits\/s//")

  # Now we expect 
	assert [ $(echo "${stripped} > 0" | bc) ]

	assert [ $(find ${RECDIR}/${SN3}.ts -type f -size +1024c) ]
}

# TODO
@test "should record during scheduled time, but not after stream end" {
	echo "declare -A TIMINGS_unittest2_t1_session1=(\
	  [stream_start]=$($DATE -d '2 minutes' +$DATEFORMAT)\
	 [session_start]=$($DATE -d '60 minutes'     +$DATEFORMAT)\
	   [session_end]=$($DATE -d '120 minutes'     +$DATEFORMAT)\
	    [stream_end]=$($DATE -d '180 minutes'     +$DATEFORMAT)\
	)" > /opt/isha-streaming/sessions/unittest.sh
	
	#chmod +x /tmp/mocksession.sh
	export SESSION_FOLDER=/tmp/mocksession.sh

	SN3=unittest2_t1_stream1_session1

	source "/opt/isha-streaming/isha-common.sh"

	DEST="rtmp://localhost:1935/testapp/$SN3"
	$FFMPEGSOURCE/ffmpeg $FFMPEGARGS $DEST &
	#sleep 3

	run /opt/isha-streaming/isha-record.sh $SN3 testapp &

	sleep 10

	read_progress_stats $(record_progress_filename $SN3)
 
  
  stripped=$(echo $BITRATE | sed "s/kbits\/s//")
  # We should not have anything recorded
	assert [ ! $(echo "${stripped} > 0" | bc) ]

	# Move time forward by 3 minutes
	run faketime -f '+3m' /opt/isha-streaming/isha-record.sh $SN3 testapp &

	sleep 10

	read_progress_stats $(record_progress_filename $SN3)
 
  stripped=$(echo $BITRATE | sed "s/kbits\/s//")

  # Now we expect 
	assert [ $(echo "${stripped} > 0" | bc) ]
	
	assert [ $(find ${RECDIR}/${SN3}.ts -type f -size +1024c) ]
}

@test "should record from an unknown stream name" {
	source "/opt/isha-streaming/isha-common.sh"

	DEST="rtmp://localhost:1935/testapp/$SN2"
	$FFMPEGSOURCE/ffmpeg $FFMPEGARGS $DEST &
	sleep 3

	echo "Before run"
	run /opt/isha-streaming/isha-record.sh $SN2 testapp &

	sleep 10

	read_progress_stats $(record_progress_filename $SN2)
 
  echo "BITRATE = ${BITRATE}"
  stripped=$(echo $BITRATE | sed "s/kbits\/s//")
	assert [ $(echo "${stripped} > 0" | bc) ]
	assert [ $(find ${RECDIR}/${SN2}.ts -type f -size +1024c) ]
}

@test "should append to previous after resuming (unknown stream)" {
	source "/opt/isha-streaming/isha-common.sh"

	DEST="rtmp://localhost:1935/testapp/$SN2"
	$FFMPEGSOURCE/ffmpeg $FFMPEGARGS $DEST &
	sleep 3

	echo "Before run"
	run /opt/isha-streaming/isha-record.sh $SN2 testapp &
	sid=$!
	sleep 10
	kill $sid

	read_progress_stats $(record_progress_filename $SN2)

	first_out_time_ms=$OUT_TIME_MS

	run /opt/isha-streaming/isha-record.sh $SN2 testapp &
	sid=$!
	sleep 3
	kill $sid

	read_progress_stats $(record_progress_filename $SN2)
	second_out_time_ms=$OUT_TIME_MS

	assert [ $(echo "${second_out_time_ms} > ${first_out_time_ms}" | bc) ]

  stripped=$(echo $BITRATE | sed "s/kbits\/s//")
	assert [ $(echo "${stripped} > 0" | bc) ]

	assert [ $(find ${RECDIR}/${SN2}.ts -type f -size +1024c) ]
}

@test "should append to previous after resuming (known stream)" {
	echo "declare -A TIMINGS_unittest2_t1_session1=(\
	  [stream_start]=$($DATE -d '5 minutes ago' +$DATEFORMAT)\
	 [session_start]=$($DATE -d '60 minutes'     +$DATEFORMAT)\
	   [session_end]=$($DATE -d '120 minutes'     +$DATEFORMAT)\
	    [stream_end]=$($DATE -d '180 minutes'     +$DATEFORMAT)\
	)" > /opt/isha-streaming/sessions/unittest.sh


	source "/opt/isha-streaming/isha-common.sh"

	DEST="rtmp://localhost:1935/testapp/$SN3"
	$FFMPEGSOURCE/ffmpeg $FFMPEGARGS $DEST &
	sleep 3

	echo "Before run"
	run /opt/isha-streaming/isha-record.sh $SN3 testapp &
	sid=$!

	sleep 20
	kill $sid

	read_progress_stats $(record_progress_filename $SN3)

	first_out_time_ms=$OUT_TIME_MS

	run /opt/isha-streaming/isha-record.sh $SN3 testapp &
	sid=$!
	sleep 20
	kill $sid

	read_progress_stats $(record_progress_filename $SN3)
	second_out_time_ms=$OUT_TIME_MS

	assert [ $(echo "${second_out_time_ms} > ${first_out_time_ms}" | bc) ]

  stripped=$(echo $BITRATE | sed "s/kbits\/s//")
	assert [ $(echo "${stripped} > 0" | bc) ]

	# Check the file actually exists
	assert [ $(find ${RECDIR}/${SN3}.ts -type f -size +1024c) ]
}

# @test "should post stopped msg even when killed" {
# 	echo "todo"
# }

teardown() {
	rm -f /opt/isha-streaming/sessions/unittest.sh
	# Kill all spawned processes
	pkill -P $$ || true
}

todo_only_one_player_instance() {
	#play_stream
  assert "test true" "false"
}

todo_log_dir_permissions_ok() {
  assert "test -x $LOGDIR" "$LOGDIR should be executable"
}