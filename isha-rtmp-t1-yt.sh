#!/bin/bash
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$script_dir/isha-common.sh"

#### Parse params

usage() { 
    echo "usage: isha-rtmp -s <source file in recdir w/ extension> -d <dest rtmp server url>" >&2
    echo "                 -t <target stream name> [-f <from>] [-l] -r <time in seconds to keep retrying>" >&2;
    echo ""
    echo "Placeholder option (-p): Create pidfile under *_placeholder.pid instead of *_player.pid 
    so it can be separately cleared by other processes. 
    Also defaulting to $RECDIR/$PLACEHOLDER as source unless source file (-s) specified"
    exit 1; 
}

retrytime=0

while getopts ":s:d:t:f:lp" o; do
    case "${o}" in
        s)
            source_filename=${OPTARG}
            ;;
        d)
            dest_url=${OPTARG}
            ;;
        t)
            target_sn=${OPTARG}
            ;;
        f)
            from=${OPTARG}
            ;;
        l)
            loop=1
            ;;
        p)
            placeholder=1
            [ -z "$source_filename" ] && source_filename=$PLACEHOLDER
            ;;
        r)
            retrytime=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${source_filename}" ] || [ -z "${dest_url}" ] || [ -z "${target_sn}" ]; then
    usage
fi

if [[ -n $loop ]]; then
    FFMPEG_LOOP="-stream_loop -1"
fi

slog=$LOGDIR/${target_sn}_rtmp.log

FFMPEG_FROM=""
if [[ -n $from ]]; then
    FFMPEG_FROM="-ss $from"
fi

#### Placeholder logic

placeholder_pidfile=$(placeholder_pid_filename $target_sn)
if [[ -z $placeholder ]]; then
    # Running with a normal video, so clear out any placeholder running for $target_sn
    echo "Ensuring process from $placeholder_pidfile is cleared" >> $slog
    ensure_pidfile_cleared_or_fail $placeholder_pidfile

    PIDFILE=$(player_pid_filename $target_sn)
else
    # So we don't need to clear out existing placeholder in this case
    # If placeholder is already running, the lock acquisition will fail
    
    PIDFILE=$placeholder_pidfile
fi


#### Lockfile logic

lockfile=$(lockfile_for_sn_and_dest $target_sn $dest_url    ) || {
    msg="While determining lockfile: $lockfile"
    handle_error "$msg" && echo "$msg"
    exit 1
}
echo "Attempting to acquire lockfile $lockfile" >> $slog
acquire_lock_or_fail $lockfile

on_die ()
{
    log_info "RTMP player with pid $$ stopped playing $source_filename to $dest_url/$target_sn"
    
    # remove lock
    flock --unlock 4

    rm $PIDFILE

    # kill ffmpeg process
    pkill -P $$

    exit 0
}
trap 'on_die' EXIT

echo "$$" > $PIDFILE
chmod 777 $PIDFILE


#### EXECUTE PLAYER

log_info "RTMP player with pid $$ starting to play source $source_filename to $dest_url/$target_sn"
log_info "Waiting until file $RECDIR/$source_filename available"

if [[ "$source_filename" == rtmp://* ]]; then
    INPUT_PARAM=$source_filename
else
    INPUT_PARAM=$RECDIR/$source_filename
    until [ -f $INPUT_PARAM ]
    do
         sleep 2
         log_info "File $INPUT_PARAM not present, waiting"
         echo "rechecking..." >> $slog
    done
fi

PROGRESSFILE=$LOGDIR/${target_sn}_rtmp_progress.log
cmd="$FFMPEGSOURCE/ffmpeg -y -hide_banner -loglevel error -progress $PROGRESSFILE -nostdin $FFMPEG_LOOP -fflags +igndts  -re $FFMPEG_FROM -i $INPUT_PARAM -c copy -fflags +genpts -f flv $dest_url/$target_sn"

elapsed=0
start_time=$(date +%s)

# Loop this until more time has elapsed then specified in retrytime (in seconds)
while [ "$elapsed" -le "$retrytime" ]
do
    echo "$(date +%c) RTMP Player starting output of $source_filename to $dest_url with $cmd"

    $cmd 2>>$LOGDIR/isha-monitoring.log &
    
    # Slow down spawning a new process
    sleep 5
    
    wait

    end_time=$(date +%s)
    elapsed=$(( end_time - start_time ))
done

echo "$(date +%c) RTMP Player finished playing $source_filename with exit status $?" >> $slog